trigger ContactTrigger on Contact (after insert, after update) {
	if (trigger.isAfter) {
		
		List<Id> contactIds = new List<Id>();
		for (Contact contact : trigger.new) {
			if (trigger.isInsert) {
				if (trigger.isInsert && !String.isBlank(contact.MailingPostalCode) && contact.MailingLatitude == null && contact.MailingLongitude == null)
					contactIds.add(contact.Id);
				if (trigger.isUpdate && contact.MailingPostalCode != trigger.oldMap.get(contact.Id).MailingPostalCode)
					contactIds.add(contact.Id);
			}
		}
		if (!contactIds.isEmpty() && !Test.isRunningTest())
			Geolocation.GetContactLocation(contactIds);
	}
}