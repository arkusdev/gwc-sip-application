trigger AttachmentsCopyDocusignFileTrigger on Attachment (after insert) {
    
     List<ID> attchIds = new List<ID>();
     for(Attachment attch:Trigger.New){
         System.debug('TriggerNew: '+Trigger.New);
         attchIds.add(attch.ID);
     }
     if (attchIds.size() > 0) {
         System.debug('In Trigger: '+attchIds[0]);
         AttachmentDocusignHandler.copyFileToParent(attchIds);
     }
}