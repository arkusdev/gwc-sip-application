trigger OpportunityGrantAllocation on Opportunity (after insert) {
List <Grant_Allocation__c> lstga = new List<Grant_Allocation__c>();
    
    for (Opportunity opp : trigger.New){
    
        if(opp.OppRecordType__c.containsIgnoreCase('Grant')){
           Grant_Allocation__c ga = new Grant_Allocation__c();
           ga.Opportunity__c = opp.id;
           ga.Expected_Revenue_Amount__c = opp.Probable_Amount__c;
           if(opp.Donation_Type__c != 'Multiple Allocation') {
               ga.Program_Type__c = opp.Donation_Type__c;
           }
           lstga.add(ga);
        }
    }
    insert lstga;
}