trigger SIP_ProgramTrigger on SIP_Program__c (after insert, after update) {
	if (trigger.isAfter) {
		
		List<Id> programIds = new List<Id>();
		for (SIP_Program__c program : trigger.new) {
			if (trigger.isInsert) {
				if (trigger.isInsert && !String.isBlank(program.SIP_Classroom_Address_ZIP_CODE__c) && program.Geolocation__latitude__s == null && program.Geolocation__longitude__s == null)
					programIds.add(program.Id);
				if (trigger.isUpdate && program.SIP_Classroom_Address_ZIP_CODE__c != trigger.oldMap.get(program.Id).SIP_Classroom_Address_ZIP_CODE__c)
					programIds.add(program.Id);
			}
		}
		if (!programIds.isEmpty() && !Test.isRunningTest())
			Geolocation.GetProgramLocation(programIds);
	}
}