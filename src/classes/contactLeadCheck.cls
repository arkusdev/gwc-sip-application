public with sharing class contactLeadCheck {
	
    public class leadCheck {
        @InvocableVariable(label='contactid')
        public Id contactid;
    }
	@InvocableMethod(
       label='Check for Matching Lead'
       description='Check leads for matching first,last,email')
       
     public static void checkForMatchingLead(List<leadCheck> requests){               
       for (leadCheck req : requests) {
           system.debug('request='+req);
           checkForLead.checkingLeads(req.contactid);
       }
     }
}