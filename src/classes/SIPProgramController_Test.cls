@isTest
public class SIPProgramController_Test {
  static testMethod void test() {  
      
   		SIP_Program__c pgm1 = new SIP_Program__c(Name = '2016_Accenture_ATL', Applicant_Count__c = 222);
   		SIP_Program__c pgm2 = new SIP_Program__c(Name = '2016_GE_ATL', Applicant_Count__c = 172);
    
      	insert pgm1;
        insert pgm2;
      	Contact con1 = new Contact(FirstName = 'Victoria',LastName= 'Hugo', Email='vvvv@hhhhh.com',SIP_Stage__c ='Classroom Designations',
                             FLTaskProgress__c=4);
      	Contact con2 = new Contact(FirstName = 'Georgia',LastName='Hugonaught', Email='vvvv@hhuhuhh.com',SIP_Stage__c ='Classroom Designations',
                             FLTaskProgress__c=4);
      	insert con1;
      	insert con2;
      	SIP_Application__c c1App = new SIP_Application__c(Program_City__c = 'Atlanta', Program_City_2nd_Choice__c = 'Chicago',
                                                         ReviewScoreExt1__c = 27.25, ReviewScoreInt1__c = 44.30,Applicant__c = con1.Id,
                                                         Parent_Guardian_1_First_Name__c  ='Mac', Parent_Guardian_1_Last_Name__c ='Macnamara',
                                                         Parent_Guardian_1_Email__c = 'mmmm@mmmm.com');
      	SIP_Application__c c2App = new SIP_Application__c(Program_City__c = 'Atlanta', Program_City_2nd_Choice__c = 'Chicago',
                                                         ReviewScoreExt1__c = 21.25, ReviewScoreInt1__c = 54.30,Applicant__c = con2.Id,
                                                         Parent_Guardian_1_First_Name__c  ='Mac', Parent_Guardian_1_Last_Name__c ='Macnamara',
                                                         Parent_Guardian_1_Email__c = 'mmmm@mmmm.com');
  		insert c1App;
      	insert c2App;
      
      	SIP_Student__c par1 = new SIP_Student__c(Participant__c = con1.Id, SIP_Program__c = pgm1.Id, Participant_Type__c = 'SIP_Applicant');
		SIP_Student__c par2 = new SIP_Student__c(Participant__c = con1.Id, SIP_Program__c = pgm1.Id, Participant_Type__c = 'SIP_Applicant');
      	insert par1;
      	insert par2;
      
      
       //SIPProgramController controller = new SIPProgramController();
      
      List<SIP_Program__c> pgms = SIPProgramController.getPrograms();
      SIP_Program__c p = SIPProgramController.getProgramByName(pgm1.Name);
      List<Contact> cons =  SIPProgramController.getFullStudentsByProgram(pgm1.Name, 'Classroom Designations');
      
  }
}