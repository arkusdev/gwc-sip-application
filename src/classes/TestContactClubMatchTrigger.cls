@isTest(SeeAllData=true)
public class TestContactClubMatchTrigger 
{
    static testmethod void test_ClubMatchTrig () 
    {
         Contact ct = new Contact();
         ct.Volunteer_Status__c = 'Application Approved';
         ct.Contact_Type__c = 'Volunteer';
         ct.FirstName = 'TestJane';
         ct.LastName = 'TestDoe';
         ct.Aliases__c = 'tftl';
         ct.BirthDate = Date.newInstance(1990,1,20);
         ct.Gender__c = 'Male';
         ct.SSN__c = '900119999';
         ct.Email = 'tftl@testmail.com';
         ct.HomePhone = '4088881111';
         
         //-- Locations willing to travel (matched location)
         
         //ct.Locations_willing_to_travel_to__c = 'Atlanta - Ivy Preparatory Girls Academy';
         ct.Locations_willing_to_travel_to__c = 'American High School';
        
         ct.Days_Available__c = 'Sunday;Monday;Tuesday';
         ct.Times_Available__c = '9a - 12p;12p - 3p';
        
         ct.MailingStreet = '1 Hurry Street';
         ct.MailingCity = 'Santa Maria';  
         ct.MailingState = 'Arizona' ;
         ct.MailingCountry = 'US';
         ct.MailingPostalCode = '89991' ;
         ct.Training_Session_Date__c = Date.newInstance(2015,1,20);
         ct.RecordTypeId = [SELECT ID FROM RECORDTYPE WHERE sObjectType='CONTACT' and NAME='GENERAL CONTACT'].ID;
         insert ct;
         System.Assert(ct.Id != null, 'The Test Volunteer did not insert properly');
        
         //ct.Locations_willing_to_travel_to__c = 'Atlanta - Ivy Preparatory Girls Academy' +';'+'Venice - Jacaranda Public Library';
         ct.Locations_willing_to_travel_to__c = 'American High School' +';'+'Erie Middle School';
         update ct;
         System.Assert(ct.Id != null, 'The Test Volunteer did not update properly');
        
        //-- Unmatched location test

         Contact ct1 = new Contact();
         ct1.Volunteer_Status__c = 'Application Approved';
         ct1.FirstName = 'TestJohn';
         ct1.LastName = 'TestSmith';
         ct1.Aliases__c = 'tftl';
         ct1.BirthDate = Date.newInstance(1990,1,20);
         ct1.Gender__c = 'Male';
         ct1.SSN__c = '900119999';
         ct1.Email = 'tft2l@testmail.com';
         ct1.HomePhone = '4088881111';
         
         //-- Locations willing to travel (unmatched location)
         
         ct1.Locations_willing_to_travel_to__c = 'American High School';
        
         ct1.MailingStreet = '1 Hurry Street';
         ct1.MailingCity = 'Santa Maria';  
         ct1.MailingState = 'Arizona' ;
         ct1.MailingCountry = 'US';
         ct1.MailingPostalCode = '89991' ;
         ct1.Training_Session_Date__c = Date.newInstance(2015,1,20);
         ct1.RecordTypeId = [SELECT ID FROM RECORDTYPE WHERE sObjectType='CONTACT' and NAME='GENERAL CONTACT'].ID;
         insert ct1;
         System.Assert(ct1.Id != null, 'The Test Volunteer did not insert properly');
	}
}