global class GetContactLocationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	List<Id> contactIds;
	String query;
	
	global GetContactLocationBatch(List<Id> contactIds) {
		this.contactIds = contactIds;
	}


	global Database.QueryLocator start(Database.BatchableContext BC) {
		if (this.contactIds != null) {
			query = 'select MailingPostalCode from Contact where Id IN (\'' + String.join(this.contactIds, '\',\'') + '\')';
		} else {
			query = 'select MailingPostalCode from Contact where MailingLatitude = null and MailingLongitude = null and MailingPostalCode != null';
		}
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<Contact> contactList = (List<Contact>)scope;
		for (Contact contact : contactList) {
			System.Location location 	= Geolocation.GetLocation(contact.MailingPostalCode);
			if (location != null) {
				contact.MailingLatitude 	= location.getLatitude();
				contact.MailingLongitude 	= location.getLongitude();
			}
		}
		update contactList;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}