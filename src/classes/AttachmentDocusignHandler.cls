public class AttachmentDocusignHandler {

    public static void copyFileToParent(ID[] attachmentsid){
    
        System.Debug('In AttachmentHandler '+attachmentsid[0]);
        List<Attachment> copyattachments = new List<Attachment>();
        List<dsfs__DocuSign_Recipient_Status__c> drsUpdate = new List<dsfs__DocuSign_Recipient_Status__c>();
        Set<dsfs__DocuSign_Recipient_Status__c> drsUpdateSet = new Set<dsfs__DocuSign_Recipient_Status__c>();
        for(ID attchid: attachmentsid){
            System.Debug('AttachmentID : '+attchid);
            
            //Select the Attachment
            Attachment[] attch = [SELECT ID,name,body, ParentId FROM Attachment where id = :attchid];
            for(Attachment at: attch){
                //Test if Docusign Attachment
                if(at.ParentId.getSObjectType() == dsfs__DocuSign_Status__c.getSObjectType()){
                    Attachment newFile;// = at.clone();
                    String parentID = at.ParentId;
                    
                    dsfs__DocuSign_Recipient_Status__c drs =  [SELECT ID, Copied_To_Parent__c, Name, dsfs__Parent_Status_Record__c, dsfs__DocuSign_Recipient_Email__c 
                                              FROM  dsfs__DocuSign_Recipient_Status__c 
                                              where dsfs__Parent_Status_Record__c = :parentID and Copied_To_Parent__C = false LIMIT 1];
                    //On the parent recipient Status object we have the contact name and email to test against.
                    if(drs != null){
                        String name = drs.Name;
                        String recipientEmail = drs.dsfs__DocuSign_Recipient_Email__c;
                        Set<Contact> cons = new set <Contact>([SELECT name, email, id FROM contact 
                                           WHERE email = :recipientEmail
                                           AND name = :name]);
            
                        for(Contact c: cons){
                            if(c.Email == recipientEmail && c.Name == name){
                            //update the copied flag in the Recipient Status Object
                                drs.Copied_To_Parent__c = true;
                                drsUpdateSet.add(drs);
                                
                            //return the parent contact ID
                            //return c.id;
                                newFile = at.clone();
                                newFile.ParentId = c.id;
                                copyattachments.add(newFile);
                            }
                        }   
                    }       
                }
            }  
        }
        if(copyattachments.size() > 0){
            insert copyattachments;
        }
        if(drsUpdateSet.size() > 0){
            drsUpdate.addall(drsUpdateSet);
            update drsUpdate;
        }
    }
}