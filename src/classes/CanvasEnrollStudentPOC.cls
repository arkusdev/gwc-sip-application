public with sharing class CanvasEnrollStudentPOC {
     //Defining and passing variables to be available thru the Process Builder flow
      public class createStudentUser {
        @InvocableVariable(label='firstname')
        public string fname;
        @InvocableVariable(label='lastname')
        public string lname;
        @InvocableVariable(label='shortname')
        public string short_name;
        @InvocableVariable(label='uniqueID')
        public string unique_id;
        @InvocableVariable(label='email')
        public string email;
        
      }
    
     //Method to be invoked from the Process Builder flow
     @InvocableMethod(
       label='Canvas Enrollment'
       description='Will send student information to Canvas to create record')
    
     public static void createStudentUser(List<createStudentUser> requests) {                  
         for (createStudentUser req : requests) {
             system.debug('request='+req);
             if (req.fname.length()>0 && req.lname.length()>0 && req.short_name.length() >0 && req.unique_id.length()>0){
                 CanvasAPICalloutMethods.createStudentUser_CalloutFromFuture(req.fname, req.lname, req.short_name, req.unique_id, req.email);
             }
         }
     }
    
    //TO DO: If Student needs to be enrolled into Club, call the method on CanvasAPICalloutMethods
    //public static void enrollInClub(){
     
    //   CanvasAPICalloutMethods.enrollInClub_Callout() //Verify parameters, if any
    // }
}