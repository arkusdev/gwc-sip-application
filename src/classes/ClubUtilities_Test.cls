@IsTest
private class ClubUtilities_Test {

    static testMethod void CreateStudentEnrollmentFormTest() {    
    
        Contact studentUser = new Contact();
        Contact parentUser = new Contact();
        Club_Student_Enrollment_POC__c enroll = new Club_Student_Enrollment_POC__c();
        enroll.Student_First_Name__c = 'Anthony';
        enroll.Student_Last_Name__c = 'Soprano';
        enroll.Student_Email__c = 'TSTSTSTS@TSTSTS.com';
        enroll.Parent_First_Name__c = 'Tony';
        enroll.Parent_Last_Name__c = 'Soprano';
        enroll.Parent_Email__c = 'TSTSTSTS@TSTSTSyyy.com';
        studentUser.firstname = 'Anthony';
        studentUser.lastname = 'Soprano';
        studentUser.email  = 'TSTSTSTS@TSTSTS.com';
        //studentUser.Recordtypeid = '012J00000005Sjn'; //Sandbox id
        studentUser.Recordtypeid = '012G0000001NIrl'; //Prod id
        parentUser.firstname = 'Tony';
        parentUser.lastname = 'Soprano';
        parentUser.email  = 'TSTSTSTS@TSTSTSyyy.com';
       

        insert studentUser;
        insert parentUser;
        insert enroll;
       
       String SIS;
       try{
              SIS =  ClubUtilities.getContactID(enroll.Student_Email__c);
          }
       catch(Exception e) {
              System.debug('Club Utilities Error:' + e);
          }     
     //   SYSTEM.assertEquals(SIS, studentUser.ID);
        try{
         SIS =  ClubUtilities.getGeneralContactID('TSTSTSTS@TSTSTSyyy.com');
        }
        catch(Exception e) {
              System.debug('Club Utilities Error2:' + e);
          }  
    }
}