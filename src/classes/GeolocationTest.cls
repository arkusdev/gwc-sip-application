@isTest
private class GeolocationTest {
	
	static testMethod void GetContactLocationTest() {
		Test.setMock(HttpCalloutMock.class, new GoogleMapsGeocodingAPIMockGenerator.GetGeocodeByPostalCode());
		Contact contact = new Contact();
		contact.LastName = 'Test';
		contact.MailingPostalCode = '12900';
		insert contact;

		Test.startTest();
			Geolocation.GetContactLocation(new List<Id>{contact.Id});
		Test.stopTest();

		contact = [select MailingLatitude, MailingLongitude from Contact where Id =: contact.Id];
		system.assert(contact.MailingLatitude != null);
		system.assert(contact.MailingLongitude != null);
	}

	static testMethod void GetProgramLocationTest() {
		Test.setMock(HttpCalloutMock.class, new GoogleMapsGeocodingAPIMockGenerator.GetGeocodeByPostalCode());
		List<Region__c> regionList = new List<Region__c>();
		Region__c region1 = new Region__c(Name = '1');
		insert region1;

		Metro_Area__c market1 = new Metro_Area__c();
		market1.Name = 'Market1';
		market1.Region__c = region1.Id;
		insert market1;

		SIP_Program__c program1 = new SIP_Program__c();
		program1.Name = 'Program1';
		program1.Metro_Area__c = market1.Id;
		program1.Year_Active__c = '2017';
		program1.SIP_Classroom_Address_ZIP_CODE__c = '12900';
		insert program1;

		Test.startTest();
			Geolocation.GetProgramLocation(new List<Id>{program1.Id});
		Test.stopTest();

		program1 = [select Geolocation__latitude__s, Geolocation__longitude__s from SIP_Program__c where Id =: program1.Id];
		system.assert(program1.Geolocation__latitude__s != null);
		system.assert(program1.Geolocation__longitude__s != null);
	}
}