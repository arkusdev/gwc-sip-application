@isTest
public with sharing class ApplicationGWCParticipationHander_TEST {

    static TestMethod void CreateApplicantGWCRelation() {
       
       Lead duplead = new Lead(); 
       duplead.firstname = 'James';
       duplead.email = 'lslkslk@lskjafs.com';
       duplead.lastname = 'Van Gleason';
       duplead.company = 'Van Gleason' ;
       duplead.Status = 'Open - Not Contacted';
       insert duplead;
       Contact student = new Contact();
       SIP_Program__c sip1 = new SIP_Program__c();
       SIP_Program__c sip2 = new SIP_Program__c();
       SIP_Program__c sip3 = new SIP_Program__c();
       SIP_Program__c sip4 = new SIP_Program__c();
       student.firstname = 'James';
       student.email = 'lslkslk@lskjafs.com';
       student.lastname = 'Van Gleason';
       student.FLTaskProgress__c = 3;
        
       sip1.Application_Form_Label__c = '405 Howard Street, San Francisco, CA 94105 (June 13 - July 29)';
       sip1.name = '2016_sip1_NYC';
       sip2.Application_Form_Label__c = '345 Park Avenue, San Jose, CA 95110-2704 (June 13 - August 5)';
       sip2.name = '2016_sip1_NYC';
       sip3.Application_Form_Label__c = '1200 Park Avenue, Emeryville, CA 94608 (June 27 - August 12)';
       sip3.name = '2016_sip1_NYC';
       sip4.Application_Form_Label__c = '601 Townsend Street, San Francisco, CA 94103 (June 20 - August 12)';
       sip4.name = '2016_sip1_NYC';
       sip1.Applicant_count__c = 0;
       sip2.Applicant_count__c = 0;
       sip3.Applicant_count__c = 0;
       sip4.Applicant_count__c = 0;
        

       insert student;
       insert sip1;
       insert sip2;
       insert sip3;
       insert sip4;
           
        List<ApplicationGWCParticipant.createGWCParticipation> participantList = new List<ApplicationGWCParticipant.createGWCParticipation>();
        ApplicationGWCParticipant.createGWCParticipation participant = new ApplicationGWCParticipant.createGWCParticipation();
       
        participant.applicantfirstname = 'James';
        participant.applicantlastname = 'Van Gleason';
        participant.applicantemail = 'lslkslk@lskjafs.com';
        participant.sipaddresschoices =  '405 Howard Street, San Francisco, CA 94105 (June 13 - July 29),345 Park Avenue, San Jose, CA 95110-2704 (June 13 - August 5)';
        participant.sipaddresschoices2 = '601 Townsend Street, San Francisco, CA 94103 (June 20 - August 12),1200 Park Avenue, Emeryville, CA 94608 (June 27 - August 12)';
        participantList.add(participant);
       
        ApplicationGWCParticipant.createGWCParicipation(ParticipantList);
        //Run twice to go through delete  code
       ApplicationGWCParticipant.createGWCParicipation(ParticipantList);
    }
}