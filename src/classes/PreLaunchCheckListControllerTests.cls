@isTest
public class PreLaunchCheckListControllerTests {
	static testMethod void testPreLaunchController() {
        PageReference pageRef = Page.PreLaunchCheckList;
        ApexPages.currentPage().getParameters().put('clubid', 'C-000001267');
    	Pre_Launch_Checklist__c plcl = new Pre_Launch_Checklist__c();
        PreLaunchCheckListController controller = new PreLaunchCheckListController();
    	Test.startTest();
        controller.preLaunchCheckList();
        plcl.First_Name__c = 'Quentin';
        plcl.Last_Name__c = 'Crisp';
        plcl.Host_Name__c = 'My Place';
        plcl.Club_id__c = 'C-000001267';
        plcl.Role_in_Club__c = 'Advisor';
        plcl.Days_Held__c = 'Friday';
        plcl.Launch_date__c = Date.newInstance(2015,9,20);
        plcl.Girls_In_Club__c = 22;
        plcl.Time_Held_Begin__c = '9:30am';
        plcl.Time_Held_End__c = '11:00am'; 
        controller.save();
        controller.showFailedPopup();
        controller.closeFailedPopup();
        controller.showPopup();
        controller.closePopup();
        controller.Reset();
       
    	Test.stopTest();
    	//System.assertNotEquals(null, nextPage);
	}
}