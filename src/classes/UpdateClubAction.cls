//
// (c) 2015 Salesforce.com
//
// Updates Club record whenever Pre Launch Checlist object record is created or edited 
// Called from Update Club Record Process Flow
//
// July 2015     Created by Vishwas Karhade
//
global class UpdateClubAction {
  @InvocableMethod(label='Update Club')
  global static List<UpdateClubActionResult> updateClub(List<UpdateClubActionRequest> requests) {
    List<UpdateClubActionResult> results = new List<UpdateClubActionResult>();
    for (UpdateClubActionRequest request : requests) {
      results.add(updateClub(request));
    }
    return results;
  }

  public static UpdateClubActionResult updateClub(UpdateClubActionRequest request) {
        String plcid=request.plcid;
        Pre_Launch_Checklist__c plc=[Select Id,Name,Time_Held_Begin__c,Time_Held_End__c,Girls_in_Club__c,Days_Held__c,Launch_Date__c,club_id__c
        from Pre_Launch_Checklist__c where id=:plcid];
        
        String clubToUpdateId=plc.club_id__c;
        System.debug('Club ID to Update = '+ clubToUpdateId);
        
        Club__c clubToUpdate= [Select Id, Club_Start_Time__c,Club_End_Time__c,Club_Type__c,Girls_in_Club__c,
        Launch_Date__c,Meeting_Day__c,Pre_Launch_Form_Id__c from Club__c where Name=:clubToUpdateId];
        
        clubToUpdate.Club_Start_Time__c=plc.Time_Held_Begin__c;
        clubToUpdate.Club_End_Time__c=plc.Time_Held_End__c;
        clubToUpdate.Girls_in_Club__c=plc.Girls_in_Club__c;
        clubToUpdate.Meeting_Day__c=plc.Days_Held__c;
        clubToUpdate.Launch_Date__c=plc.Launch_Date__c;
        clubToUpdate.Pre_Launch_Form_Id__c=plc.Name;
        clubToUpdate.Pre_Launch_Questionnaire__c = true;
        update clubToUpdate;
        UpdateClubActionResult result = new UpdateClubActionResult();
        
        
        //Creating a Task every time a Pre Launch Checklist is submitted so that the submission can be tracked from Club - Start
        
        task clubTask = new task ();

        clubTask.Status = 'Completed';
        clubTask.Priority = 'Normal';
        clubTask.Subject = 'Pre Launch Checklist Submitted';
        clubTask.ActivityDate = Date.today();
        clubTask.OwnerId = '005G0000004tnB9';
        clubTask.WhatId = clubToUpdate.id;
        
        insert clubTask;
        
        //Create Task - End

        result.clubId=clubToUpdate.Id;
        return result;
  }

  global class UpdateClubActionRequest {
    @InvocableVariable
    public ID plcId;
  }

  global class UpdateClubActionResult {
    @InvocableVariable
    public ID clubId;
  }

  class UpdateClubActionException extends Exception {}
}