public with sharing class ClubEnrollStudentRelationship {
     //Defining and passing variables to be available thru the Process Builder flow
      public class createStudentRelationship {
        @InvocableVariable(label='studentfirstname')
        public string studentfname;
        @InvocableVariable(label='studentlname')
        public string studentlname;
        @InvocableVariable(label='studentemail')
        public string studentemail;
        @InvocableVariable(label='parentfirstname')
        public string parentfname;
        @InvocableVariable(label='parentlastname')
        public string parentlname;
        @InvocableVariable(label='parentemail')
        public string parentemail;
        @InvocableVariable(label='parent2firstname')
        public string parent2fname;
        @InvocableVariable(label='parent2lastname')
        public string parent2lname;
        @InvocableVariable(label='parent2email')
        public string parent2email;
        @InvocableVariable(label='Club')
        public string club;
        @InvocableVariable(label='FormId')
        public string formid;
        @InvocableVariable(label='relationship')
        public string relationship;
        @InvocableVariable(label='relationshipother')
        public string relationshipother;
        @InvocableVariable(label='relationship2')
        public string relationship2;
        @InvocableVariable(label='relationshipother2')
        public string relationshipother2;
        
      }
    
     //Method to be invoked from the Process Builder flow
     @InvocableMethod(
       label='Student Relationship'
       description='Create a Child-Parent Relationship')
    
     public static void createStudentRelationship(List<createStudentRelationship> requests) {                  
         for (createStudentRelationship req : requests) {
             system.debug('request='+req);
             if (req.studentfname.length()>0 && req.studentlname.length()>0 && req.studentemail.length() >0 ){
                 ClubEnrollStudentRelationHandler.createStudentRelationship(req.studentfname, req.studentlname, req.studentemail,
                                                                            req.parentfname, req.parentlname, req.parentemail,
                                                                            req.parent2fname, req.parent2lname, req.parent2email,
                                                                            req.club, req.formid, req.relationship,req.relationshipother,
                                                                            req.relationship2,req.relationshipother2);
             }
         }
     }

}