public class Geolocation {

	public static void GetContactLocation(List<Id> contactIds) {
		Database.executeBatch(new GetContactLocationBatch (contactIds), 100);
	}

	public static void GetProgramLocation(List<Id> programIds) {
		Database.executeBatch(new GetProgramLocationBatch (programIds), 100);
	}

	public static System.Location GetLocation(String zipcode){ 
		String apiKey = [select API_Key__c from Google_Maps_Geocoding_API__mdt where DeveloperName = 'API_Key'].API_Key__c; 
		String url = 'https://maps.googleapis.com/maps/api/geocode/xml?';  
		url += 'components=postal_code:' + zipcode;
		url += '&key=' + apiKey; 
		Http h = new Http(); 
		HttpRequest req = new HttpRequest(); 
		req.setHeader('Content-type', 'application/x-www-form-urlencoded'); 
		req.setHeader('Content-length', '0'); 
		req.setEndpoint(url); 
		req.setMethod('POST'); 
		HttpResponse res = h.send(req); 
		return parseBodyIntoLocation(res);
	} 

	private static Location parseBodyIntoLocation(HttpResponse response) { 
		Decimal lat;
		Decimal lng;
		try {
		    Dom.Document doc = response.getBodyDocument();
		    Dom.XMLNode geocode = doc.getRootElement();
		    for(Dom.XMLNode child : geocode.getChildElements()) {  
				if (child.getName() == 'result') {
				    for (Dom.XMLNode result : child.getChildElements()) {
				        if (result.getName() == 'geometry') {
				            for (Dom.XMLNode geometry : result.getChildElements()) {
				                if (geometry.getName() == 'location') {    
				                    for (Dom.XMLNode location : geometry.getChildElements()) {
				                        if (location.getName() == 'lat') {
				                	       lat = Decimal.valueOf(location.getText());
				                        }
						                if (location.getName() == 'lng') {
						                	lng = Decimal.valueOf(location.getText());
						                }
						            }
						        }
				            }
				        }
				    }
				}
			}
		} catch (Exception e) {
			
		}
		System.Location location;
		if (lat != null && lng != null)
			location = System.Location.newInstance(lat,lng);
		return location;
	}


}