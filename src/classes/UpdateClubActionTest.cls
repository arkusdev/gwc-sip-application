@IsTest

private class UpdateClubActionTest {

    static testMethod void updateClubTest() {       
       
       List<UpdateClubAction.UpdateClubActionRequest> actionRequests = new List<UpdateClubAction.UpdateClubActionRequest>();
       
       List<UpdateClubAction.UpdateClubActionResult> actionResults = new List<UpdateClubAction.UpdateClubActionResult>();                              
       //create an organization for the club
       Account acct = new Account();
       acct.name = 'Club Host';
       insert acct;
       //create the Club object
       Club__c clubObjIns = new Club__c();
               
        clubObjIns.Host_Organization__c = acct.id;
        clubObjIns.Club_Name__c = 'Dummy Club';
        clubObjIns.Meeting_Day__c = 'Sunday';        
        clubObjIns.Girls_In_Club__c = 23;        
        clubObjIns.Launch_Date__c = Date.today();        
        clubObjIns.Club_Start_Time__c = '4:30 PM';
        clubObjIns.Club_End_Time__c = '6:30 PM';
       
        insert clubObjIns;  
       
       Club__c clubObj = new Club__c();
       
       clubObj = [Select Name from Club__c order by LastModifiedDate desc limit 1];
       
       
       //Create the PLC object
       Pre_Launch_Checklist__c plcObj = new Pre_Launch_Checklist__c();       
        plcObj.Club_Id__c = clubObj.Name;
        
        System.Debug(clubObj.Name);
        
        plcObj.Days_Held__c = 'Wednesday, Saturday';
        plcObj.First_Name__c = 'Test First Name';
        plcObj.Girls_In_Club__c = 23;
        plcObj.Host_Name__c = 'Test Host Name';
        plcObj.Last_Name__c = 'Test Last Name';
        plcObj.Launch_Date__c = Date.today();
        plcObj.Role_In_Club__c = 'Club Faculty Advisor';
        plcObj.Time_Held_Begin__c = '12:30 PM';
        plcObj.Time_Held_End__c = '2:30 PM';
       
        insert plcObj;                      
       
       Pre_Launch_Checklist__c plcObjClub = new Pre_Launch_Checklist__c();
       
       plcObjClub = [Select Id from Pre_Launch_Checklist__c where Club_Id__c =:clubObj.Name]; 
       
       UpdateClubAction.UpdateClubActionRequest  ucar = new UpdateClubAction.UpdateClubActionRequest();
       ucar.plcId = plcObjClub.Id;       
       actionRequests.add(ucar);
       
       for (UpdateClubAction.UpdateClubActionRequest request : actionRequests) {
            //request.plcid = plcObjClub.id;              
              System.Debug(request.plcid);
              actionResults = UpdateClubAction.updateClub(actionRequests);
              System.Debug(actionResults[0].clubid);              
              System.assert(actionResults[0].clubid != null, 'something went wrong');
                         
       }
       
                               
    }

}