public with sharing class ClubStudentEnrollmentFormController {

    public PageReference getClubName() {
        return null;
    }

    public Club_Student_Enrollment_POC__c clubObj{get;set;}

    public Club_Student_Enrollment_POC__c clubStudentEnrollmentForm{get;set;}
    
    public boolean displayPopup {get; set;}
    
    public boolean displayFailedPopup {get; set;}
    
    
    public Club_Student_Enrollment_POC__c clubStudentEnrollmentForm() {
        return clubStudentEnrollmentForm;
    }         

    public ClubStudentEnrollmentFormController() {
        clubStudentEnrollmentForm = new Club_Student_Enrollment_POC__c();
            if( ApexPages.currentPage().getParameters().get('clubid') != null){
            clubStudentEnrollmentForm.Club_ID__c=ApexPages.currentPage().getParameters().get('clubid');
            
      
        }   
    }  
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('Yes','Yes'));
            options.add(new SelectOption('No','No'));
            options.add(new SelectOption('Eligible not offered','My school does not offer a free or reduced lunch program but I believe I would be eligible')); 
            options.add(new SelectOption('Unsure','Unsure')); 
            return(options);       
        }         

                       
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
     public void closeFailedPopup() {        
        displayFailedPopup = false;    
    }     
    public void showFailedPopup() {        
        displayFailedPopup = true;    
    }

       
    public PageReference save() {
       
       try{
            upsert clubStudentEnrollmentForm;
            showPopup();
            

       }
           catch(DmlException ex){
           ApexPages.addMessages(ex);

       }

    return null;

  } 
    public pagereference Reset(){
    
        if(clubStudentEnrollmentForm.Student_Under_13__c == true) {
              //Demo URL String DocuSignURL = 'https://demo.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e00ec8ab-5f8c-4c41-b123-61b98222b8aa';
               PageReference pageRef = new PageReference('https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=95b68b62-8e26-4e27-9adb-4402b196a7d6');
               Club__c[] cname =  [SELECT Club_Name__c FROM Club__c WHERE Id = :clubStudentEnrollmentForm.Club_Name__c];
               if(cname.size() > 0 ){
                  pageRef.getParameters().put('ClubId',cname[0].Club_Name__c);
               }
               pageRef.setRedirect(true);
               return pageRef;}

       else {
          PageReference pageRef = new PageReference('http://www.girlswhocode.com');
          pageRef.setRedirect(true);
                 return pageRef;}   

       //PageReference pageRef = new PageReference('http://www.girlswhocode.com');
       //pageRef.setRedirect(true);
       //return pageRef;   
      }

}