@IsTest
private class CanvasAPI_Test {
  static testMethod void callOutTest() {  
        Test.setMock(HttpCalloutMock.class, new CanvasAPICallout_MockMethods());
        canvasAPI c = new canvasAPI(); 
        
        string name = 'Kelly Thompson';
        string short_name  = 'kthompson';
        string unique_id  = 'kthompson@example.com';
        
        string method = 'POST';
        string endpoint  = 'user[name]='+name+'&user[terms_of_use]=true&user[short_name]='+short_name+'&pseudonym[unique_id]='+unique_id+'&pseudonym[force_self_registration]=true';
        string body  = 'https://girlswhocode.instructure.com/api/v1/accounts/1/users?access_token=5910~fxMFKc6OpPbXkV6XyKjA5Yuab4OYqk94PIagoqeTl580lWXV780rRGGDrh2jVSup';
  
        httpResponse res = new httpResponse();
        try{
            res = canvasAPI.callout(method, endpoint, body);
            System.assert(res != null, 'something went wrong');}
        catch(Exception e) {
            System.debug('Call Out Error from Canvas API class:' + e);}
        }            
}