@isTest
public class ClubEnrollStudentRelationHander_Test {
    static TestMethod void CreateClubStudentRelations() { 
       Contact student = new Contact();
       Contact student2 = new Contact();
       Contact parent = new Contact();
       Contact parent2 = new Contact();
       student.firstname = 'Reginald';
       student.email = 'tttt@ysysysy.com';
        
       student.lastname = 'Van Gleason';
       student2.firstname = 'Reggy';
       student2.email = 'tttt@ysysysy.com';
        
       student2.lastname = 'Gleason';
       parent.email = 'lslkslk@lskjafs.com';
       parent.firstname = 'James';
       parent.lastname = 'Van Gleason'; 
       parent2.email = 'lslkslk@lskj.org';
       parent2.firstname = 'Jinny';
       parent2.lastname = 'Van Gleason'; 
       insert student;
       insert parent;
       insert parent2;
       insert student2;
        
        Club__c c = new Club__c();
        //Create new club 
         Account o = new Account();
        //Create new account
        
        o.Name = 'Test Organization';
        o.BillingStreet = '1313 Mocking Bird Lane';
        o.BillingCity = 'Mocking Bird Nest';
        o.BillingCountry = 'Mocking Bird Island';
        o.BillingState = 'Mocking Bird Flock';
        o.BillingPostalCode = '11111';
        o.Club_Host_Location_Type__c = 'Elementary School';
        
        insert o;
       
        c.meeting_day__c = 'Monday;Tuesday';
        c.meeting_time__c = '9a - 12p;12p - 3p';
        c.host_organization__c = o.ID;
        c.club_name__c = 'Test Club';
        
        
        List <ClubEnrollStudentRelationship.createStudentRelationship> SRList = new List<ClubEnrollStudentRelationship.createStudentRelationship>();
        List <ClubEnrollStudentRelationship.createStudentRelationship> SRList2 = new List<ClubEnrollStudentRelationship.createStudentRelationship>();
        List <ClubEnrollStudentRelationship.createStudentRelationship> SRList3 = new List<ClubEnrollStudentRelationship.createStudentRelationship>();


        ClubEnrollStudentRelationship.createStudentRelationship SR = new ClubEnrollStudentRelationship.createStudentRelationship();
        SR.parentfname = 'James';
        SR.parentlname = 'Van Gleason';
        SR.parentemail = 'lslkslk@lskjafs.com';
        SR.relationship = 'Father';
        SR.relationshipother = 'PotScrubber';
        SR.parent2fname = 'Jinny';
        SR.parent2lname = 'Van Gleason';
        SR.parent2email = 'lslkslk@lskj.org';
        SR.relationship2 = 'Mother';
        SR.relationshipother2 = 'PotScrubber';
        SR.studentemail = student.Email;
        SR.studentfname = student.FirstName;
        SR.studentlname = student.LastName;
        SR.club = c.Id;
        SRList.add(SR);
        ClubEnrollStudentRelationship.createStudentRelationship(SRList);
        SR.relationship = 'Other';
        SR.relationship2 = 'Other';
        SRList2.add(SR);
        
        //Run twice to go through relationship exists code
        ClubEnrollStudentRelationship.createStudentRelationship(SRList2);
        
        //Run with missing relationship to test that case
        SR.parentfname = 'James';
        SR.parentlname = 'Van Gleason';
        SR.parentemail = 'lslkslk@lskjafs.com';
        SR.relationship = '';
        SR.relationship2 = '';
        SR.relationshipother = 'PotScrubber';
        SR.parent2fname = 'Jinny';
        SR.parent2lname = 'Van Gleason';
        SR.parent2email = 'lslkslk@lskj.org';
        SR.studentemail = student2.Email;
        SR.studentfname = student2.FirstName;
        SR.studentlname = student2.LastName;
        SRList3.add(SR);
        ClubEnrollStudentRelationship.createStudentRelationship(SRList3);
        
       // ClubEnrollStudentRelationHandler.createStudentRelationship(student.firstName,student.lastName,student.email,
       //                                                            parent.FirstName,parent.LastName,parent.Email,c.id);
    }
}