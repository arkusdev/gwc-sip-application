public class SIP {
	
	public static String 							YEAR 					{ get; set; }//YEAR selected by the UI user for querying applications and programs
	public static List<SIP_Application_Criteria__c> CRITERIA_LIST 			{ get; set; }//SIP_Application_Criteria__c record list
	public static SIP_Program__c 					CURRENT_PROGRAM 		{ get; set; }//When looping between programs this is a placeholder for the current program
	public static Map<Id, Contact> 					CONTACT_MAP 			{ get; set; }//Map for the Application Contact records, (SIP_Application__c.Applicant__c)
	public static Map<Id, SIP_Application__c>		APPLICATION_MAP			{ get; set; }//SIP_Application__c map of all the Holding Bucket applications for the YEAR selected
	public static Integer 							CLASS_CAPACITY 			{ get; set; }//How many applications a program could have assign
	public static Integer 							WAITLISTED				{ get; set; }//How many applications a program could have waitlisted
	public static Map<Id, ProgramPercentage> 		PROGRAM_PERCENTAGE_MAP 	{ get; set; }//For each program we store the percent per Percentage criteria rule that is "At Least"
	public static ProgramPercentage 				PROGRAM_PERCENTAGE 		{ get; set; }
	public static Integer 							APPLICANT_LIMIT 		{ get; set; }


	
	public static void ClassAssignment(String theYear) {
		List<SIP_Application__c> applicationList = [select Category__c from SIP_Application__c where Application_Year__c =: theYear and SIP_Stage__c = 'Holding Bucket' and Acceptance_Status_Determination__c = null and Category__c != null];
		for (SIP_Application__c application : applicationList) {
			application.Category__c = null;
			application.Acceptance_Status__c = null;
		}
		
		List<Region__c> regionList = [select WaitlistedSpots__c from Region__c where WaitlistedSpots__c != 0];
		for (Region__c region : regionList) 
			region.WaitlistedSpots__c = 0;


		List<SIP_Program__c> programList = [select Metro_Area__r.Name, 
													SIP_Application_Sorted_List__c, 
													PriorityPercentageMap__c
												from SIP_Program__c
												where Inactive__c = false
												and Year_Active__c =: theYear];
		SIP.CLASS_CAPACITY = 20;
		SIP.CRITERIA_LIST = [select At_Least_Less_Than__c,
								Field_Name__c,
								Field_Values__c,
								Object_Name__c,
								Particular_Market__c,
								Percent__c,
								Type__c,
								Priority__c,
								Target__c,
								Number_of_Spots__c
							from SIP_Application_Criteria__c
							where Active__c = true
							order by Priority__c ASC];
		Map<String, Integer> marketNameToSpots = new Map<String, Integer>();
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if (criteria.Type__c == 'Percentage' && criteria.Number_of_Spots__c != null) {
				if (criteria.Target__c == 'National') {
					Integer percentage = Integer.valueOf((criteria.Number_of_Spots__c * 100) / (SIP.CLASS_CAPACITY * programList.size()));
					criteria.Percent__c = percentage;
				} else if (criteria.Target__c == 'Market'){
					marketNameToSpots.put(criteria.Particular_Market__c, 0);
				}
			}
		}
		for (SIP_Program__c program : programList) {
			if (marketNameToSpots.containsKey(program.Metro_Area__r.Name)) {
				marketNameToSpots.put(program.Metro_Area__r.Name, marketNameToSpots.get(program.Metro_Area__r.Name) + 1);
			}
		}

		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if (criteria.Type__c == 'Percentage' && criteria.Number_of_Spots__c != null) {
				if (criteria.Target__c == 'Market') {
					Integer percentage = Integer.valueOf((criteria.Number_of_Spots__c * 100) / (SIP.CLASS_CAPACITY * marketNameToSpots.get(criteria.Particular_Market__c)));
					criteria.Percent__c = percentage;
				}
			}
		}			

		SIP_Placeholder__c placeholder = SIP_Placeholder__c.getOrgDefaults();
		placeholder.ProgramId__c = null;
		placeholder.StillApplicants__c = false;
		placeholder.RunBatchAgain__c = false;
		placeholder.Year__c = null;
		upsert placeholder;
		update applicationList;
		update regionList;
		update SIP.CRITERIA_LIST;
		if (!Test.isRunningTest())
			Database.executeBatch(new SIPApplicationSortByProgramBatch(theYear, null), 1);
	}

	//Calculates based on the new application assigned to the program where the percentage of each "Percentage" "At Least" criteria rule are on the program.
	public static void programCriteriaPercentageCalc(SIP_Program__c program, SIP_Application__c application) {
		String[] applicantSplittedValues;
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if ((String.isEmpty(criteria.Particular_Market__c) || criteria.Particular_Market__c == program.Metro_Area__r.Name) && criteria.Type__c == 'Percentage' && criteria.At_Least_Less_Than__c == 'At Least' && criteria.Target__c != 'National') {
				if (criteria.Object_Name__c == 'SIP_Application__c') {
					applicantSplittedValues = String.isBlank(String.valueOf(application.get(criteria.Field_Name__c))) ? null : String.valueOf(application.get(criteria.Field_Name__c)).split(';');
				} else if (criteria.Object_Name__c == 'Contact') {
					applicantSplittedValues = String.isBlank(String.valueOf(SIP.CONTACT_MAP.get(application.Applicant__c).get(criteria.Field_Name__c))) ? null : String.valueOf(SIP.CONTACT_MAP.get(application.Applicant__c).get(criteria.Field_Name__c)).split(';');
				}
				if (applicantSplittedValues!= null) {
					for (String value : applicantSplittedValues) {
						if (!String.isBlank(value) && criteria.Field_Values__c.contains(value)) {
							SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).calcPercentageByAddingOne(Integer.valueOf(criteria.Priority__c), Integer.valueOf(program.ApplicantAssignedCount__c), Integer.valueOf(criteria.Percent__c));
							break;
						}
					}
				}
			}
		}
	}

	public class ProgramPercentage {
		public Map<Integer, Integer> 	criteriaPriorityToNumber 		{ get; set; }
		public Map<Integer, Integer> 	criteriaPriorityToPercentage 	{ get; set; }
		public Boolean 					percentageReached				{ get; set; }
		public ProgramPercentage() {
			criteriaPriorityToNumber = new Map<Integer, Integer>();
			criteriaPriorityToPercentage = new Map<Integer, Integer>();
			percentageReached = false;
		}

		public void calcPercentageByAddingOne(Integer priority, Integer applicantCount, Integer criteriaPercentage) {
			if (criteriaPriorityToNumber.containsKey(priority)) {
				Integer count = criteriaPriorityToNumber.get(priority);
				criteriaPriorityToNumber.put(priority, count + 1);
			} else {
				criteriaPriorityToNumber.put(priority, 1);
			}
			Integer newPercentage = applicantCount < SIP.CLASS_CAPACITY ? (criteriaPriorityToNumber.get(priority) * 100) / SIP.CLASS_CAPACITY : (criteriaPriorityToNumber.get(priority) * 100) / (SIP.CLASS_CAPACITY + SIP.WAITLISTED);
			Integer oldPercentage = Integer.valueOf(criteriaPriorityToPercentage.get(priority));
			criteriaPriorityToPercentage.put(priority, newPercentage);
			if (newPercentage >= criteriaPercentage)
				percentageReached = true;
		}
	}
}