@isTest(SeeAllData=true)
public class TestOpportunityGrantAllocationTrigger {

    static testmethod void test_TestOpportunityGrantAllocationTrig () {
        
        Opportunity o = new Opportunity();
        //Create new account
        
        o.Name = 'Test Opportunity';
        o.CloseDate = date.ValueOf('2015-09-21');
        o.RecordTypeId =  [SELECT ID FROM RECORDTYPE WHERE sObjectType='OPPORTUNITY' and NAME = 'GRANT'].ID;
        o.Amount = 10000;
        o.stageName = 'Prospecting';
        
        insert o;
        System.Assert(o.Id != null, 'The Test Opportunity did not insert properly');
        
        Grant_Allocation__c ga = new Grant_Allocation__c();
        ga.Opportunity__c = o.Id;
        ga.Expected_Revenue_Amount__c = 10000;
        
        insert ga;
        System.Assert(ga.Id != null, 'The Grant Allocation did not insert properly');   
            

    }
}