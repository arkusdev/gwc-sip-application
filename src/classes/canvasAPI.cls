public with sharing class canvasAPI {
    public static string BASE_URL = 'https://girlswhocode.instructure.com/api/vi/';
    
    //Method to perform a callout and return an http response
    public static httpResponse callout(String httpMethod, String endpoint, String body){
        httpRequest req = new httpRequest();
        
        // set the http verb being used (Required) (Get, Put, Post, Delete, Head, Trace)
        req.setMethod(httpMethod);
        
        // set the URI of the 3rd party service (Required)
        req.setEndpoint(endpoint);

        // set the body of the message (Optional)
        if ( string.isNotBlank(body)){
            req.setBody(body);
            req.setHeader('Content-Length', string.valueOf(body.length()));
          //  req.setHeader('Content-Type:', 'application/json');
        }        
        
        //Optional Set the timeout duration in millseconds
        req.setTimeout(120000);
        
        //Optional Attributes required by the third party
        //req.setHeader('Accept-Encoding','gzip, deflate');
        
        //Optional Tell Apex to compress the body
        //req.setCompressed(true);
        //Use the HTTP Class to send the request and receive the httpResponse
        /*If you are not using an HttpCalloutMock:
         if (!test.isRunningTest){
        */
        httpResponse res = new http().send(req);
        /*If you are not using an HttpCalloutMock;
        }
        */
        system.debug(res.toString()); 
        system.debug(res.getBody()); 
        return res; 
        
    }

}