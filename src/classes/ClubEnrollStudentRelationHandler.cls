public class ClubEnrollStudentRelationHandler {
//Builds a parent-child relationship from the Club Student Enrollment Form  
 
    
    @future  //@Future ensures the relationship occurs after the Student & Parent Contacts have
    //committed to the database.
    public static void createStudentRelationship(string sfname, string slname, string semail, 
    string pfname, string plname, string pemail, string p2fname, string p2lname, string p2email,
    string club, string formid, string inrelationship,string relationshipother,string inrelationship2,string relationshipother2){
       
        List<Contact> StudentID = [SELECT ID FROM Contact WHERE (email = :semail AND FirstName = :sfname and LastName = :slname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        List<Contact> ParentID = [SELECT ID FROM Contact WHERE (email = :pemail AND FirstName = :pfname and LastName = :plname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        List<Contact> Parent2ID = [SELECT ID FROM Contact WHERE (email = :p2email AND FirstName = :p2fname and LastName = :p2lname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        System.debug('Club: '+ club);
        if(StudentID.size() > 0 && ParentID.size() > 0){ 
            //Check for an existing relationship so as to not duplicate
             List <npe4__Relationship__c> prel = [SELECT id, npe4__type__c, relationship_other__c from npe4__Relationship__c WHERE npe4__Contact__c = :StudentID[0].id
                                                                AND npe4__RelatedContact__c = :ParentID[0].Id ORDER BY createddate ASC NULLS FIRST LIMIT 1];
            if(prel.size() > 0){
                System.Debug('Relationship1 Exists');
                if(inrelationship != null && (inrelationship != prel[0].npe4__Type__c || relationshipother != prel[0].relationship_other__c)){
                    prel[0].npe4__Type__c = inrelationship;
                    prel[0].relationship_other__c = relationshipother;
                    update prel;
                 }
                    
            }
            else {     
                npe4__Relationship__c relationship  = new npe4__Relationship__c();
                relationship.npe4__Contact__c = StudentID[0].Id;
                relationship.npe4__RelatedContact__c = ParentID[0].Id;
                if(String.isBlank(inrelationship)) {
                     relationship.npe4__Type__c = 'Parent';
                }
                else {
                    relationship.npe4__Type__c = inrelationship; 
                }
                relationship.Relationship_other__c = relationshipother;
                relationship.npe4__Status__c = 'Current'; 
                insert relationship;
            }
                
            //Check for an existing participation so as to not duplicate
            integer cnt = 0;
            cnt = [SELECT count() from SIP_Student__c WHERE Participant__c = :StudentID[0].id
                                                                AND Club__C = :club];
            if(cnt > 0){
                System.Debug('Participation Exists');
            }
            else {

                SIP_Student__c participant = new SIP_Student__c();
                participant.Participant__c = StudentId[0].Id;
                participant.Club__c = club;
                participant.Participant_Type__c = 'Club Member';
                insert participant;
            }
            
            //Relate the Student to the enrollment form
            Club_Student_Enrollment_POC__c[] enforms = [Select Id, Participant__c 
                                                        FROM Club_Student_Enrollment_POC__c
                                                        WHERE ID = :formid];
            if (enforms.size()>0){
                enforms[0].participant__c = StudentId[0].ID;
                enforms[0].Created_Student_Relationships__c = true;
                update enforms;
            }
        }
        if(StudentID.size() > 0 && Parent2ID.size() > 0){ 
            //Check for an existing relationship so as to not duplicate
            List <npe4__Relationship__c> p2rel = [SELECT id, npe4__type__c, relationship_other__c from npe4__Relationship__c WHERE npe4__Contact__c = :StudentID[0].id
                                                                AND npe4__RelatedContact__c = :Parent2ID[0].Id ORDER BY createddate ASC NULLS FIRST LIMIT 1];
            if(p2rel.size() > 0){
                System.Debug('Relationship2 Exists');
                if(inrelationship2 != null && (inrelationship2 != p2rel[0].npe4__Type__c ||  p2rel[0].relationship_other__c != relationshipother2) ){
                    p2rel[0].npe4__Type__c = inrelationship2;
                    p2rel[0].relationship_other__c = relationshipother2;
                    update p2rel;
                 }                    
            }
            else {     
                npe4__Relationship__c relationship  = new npe4__Relationship__c();
                relationship.npe4__Contact__c = StudentID[0].Id;
                relationship.npe4__RelatedContact__c = Parent2ID[0].Id;
                if(String.isBlank(inrelationship2)) {
                     relationship.npe4__Type__c = 'Parent';
                }
                else {
                    relationship.npe4__Type__c = inrelationship2; 
                }
                relationship.relationship_other__c = relationshipother2;
                relationship.npe4__Status__c = 'Current'; 
                insert relationship;
                npe4__Relationship__c relationship2  = new npe4__Relationship__c();
                relationship2.npe4__Contact__c = Parent2ID[0].Id;
                relationship2.npe4__RelatedContact__c = StudentID[0].Id;
                relationship2.npe4__Type__c = 'Child'; 
                relationship2.npe4__Status__c = 'Current'; 
                insert relationship2;
            }                  
        }                 
     }
}