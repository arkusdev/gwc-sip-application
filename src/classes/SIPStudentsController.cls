public class SIPStudentsController {
    
@AuraEnabled
	public static List<SIP_Program__c> getSIPPrograms() {
    	return [Select Id, Name From SIP_Program__c order by Name desc];
	}


}