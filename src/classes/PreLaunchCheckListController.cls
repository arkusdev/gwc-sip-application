public with sharing class PreLaunchCheckListController {
   
    public Pre_Launch_Checklist__c preLaunchCheckList{get;set;}
    
    public boolean displayPopup {get; set;}
    
    public boolean displayFailedPopup {get; set;}
    
    public string redirectClubID;

    public PreLaunchCheckListController() {
        preLaunchCheckList = new Pre_Launch_Checklist__c();
        if( ApexPages.currentPage().getParameters().get('clubid') != null){
            preLaunchCheckList.Club_Id__c=ApexPages.currentPage().getParameters().get('clubid');
        }    
    }

    public Pre_Launch_Checklist__c preLaunchCheckList() {
        return preLaunchCheckList;
    }

    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    
     public void closeFailedPopup() {        
        displayFailedPopup = false;    
    }     
    public void showFailedPopup() {        
        displayFailedPopup = true;    
    }

    
    public PageReference save() {
       
       If (preLaunchCheckList.Club_Id__c != null) { 
            upsert preLaunchCheckList;        
            showPopup();   
            return null; }    
      else {          
          showFailedPopup();   
          return null; }       
    }
    
    public pagereference Reset(){
       
       If (preLaunchCheckList.Club_Id__c != null) { 
            redirectClubID = '/apex/PreLaunchChecklist?clubid=' + preLaunchCheckList.Club_Id__c;}
       else {    
           redirectClubID= '/apex/PreLaunchChecklist';}
           
       PageReference pg = new PageReference(redirectClubID);
       pg.setRedirect(true);
       return pg;   
      }

}