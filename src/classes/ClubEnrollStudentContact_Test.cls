@isTest
private class ClubEnrollStudentContact_Test {
   static TestMethod void CreateClubStudentContact() { 
       Lead ldstudent = new Lead();
       Lead ldparent = new Lead();
       Lead ldparent2 = new Lead();
       ldstudent.firstname = 'Reginald';
       ldstudent.email = 'tttt@ysysysy.com';
       ldstudent.Company = 'Fritz';
       ldstudent.lastname = 'Van Gleason';
       
       ldparent.email = 'lslkslk@lskjafs.com';
       ldparent.firstname = 'James';
       ldparent.lastname = 'Van Gleason'; 
       ldparent.Company = 'Fritz';
       ldparent2.email = 'janevg@lskjafs.com';
       ldparent2.firstname = 'Jane';
       ldparent2.lastname = 'Van Gleason';
       ldparent2.Company = 'Halley';
       insert ldstudent;
       insert ldparent;
       insert ldparent2;

       Test.setMock(HttpCalloutMock.class, new CanvasAPICallout_MockMethods());
       List<ClubEnrollmentStudentContact.createStudentParent> CSEList = new List<ClubEnrollmentStudentContact.createStudentParent>();
       ClubEnrollmentStudentContact.createStudentParent CSE =  new ClubEnrollmentStudentContact.createStudentParent();
       CSE.city = 'New York';
       CSE.Club = 'a0QJ0000006zKqe'; //Sandbox Club
       CSE.studentemail = 'tttt@ysysysy.com';
       CSE.studentfname =  'Reginald';
       CSE.studentlname = 'Van Gleason';
       CSE.parentemail = 'lslkslk@lskjafs.com';
       CSE.parentfname = 'James';
       CSE.parentlname = 'Van Gleason'; 
       CSE.parent2email = 'janevg@lskjafs.com';
       CSE.parent2fname = 'Jane';
       CSE.parent2lname = 'Van Gleason'; 
       CSE.state = 'NY';
       CSE.school = 'Horace Mann';
       CSE.postalcode = '10010';
       CSE.ethnicity = 'Asian';
       CSE.ifasian = 'Chinese';
       CSE.ifmultiracial = 'White;Chinese';
       CSE.freelunch = 'Yes';
       CSE.sipstudent = 'Applicant';
       CSE.returningstudent = 'No';
       CSE.birthdate = Date.newInstance( 2000, 1, 1 ) ;
       CSE.howdidyouhear = 'Event';
       CSE.howdidyouhearEVNT = 'Other';
       CSE.howdidyouhearAD = 'Facebook';
       CSE.howdidyouhearSM = 'Twitter';
       CSE.parentphone = '(777) 999-8989';
       CSE.parent2phone = '(777) 999-8989';
       CSEList.add(CSE);
       ClubEnrollmentStudentContact.createStudentParentContacts(CSEList);
    
      
       // Run the test with contact existing
       Contact student = new Contact();
       Contact parent = new Contact();
       Contact parent2 = new Contact();
       student.firstname = 'Reginald';
       student.email = 'tttt@ysysysy.com';
        
       student.lastname = 'Van Gleason';
       parent.email = 'lslkslk@lskjafs.com';
       parent.firstname = 'James';
       parent.lastname = 'Van Gleason'; 
       parent2.email = 'janevg@lskjafs.com';
       parent2.firstname = 'Jane';
       parent2.lastname = 'Van Gleason'; 

       
       insert student;
       insert parent;
       insert parent2;
       
             
       ClubEnrollmentStudentContact.createStudentParentContacts(CSEList);
      
   }
       
 
}