public class ClubEnrollmentStudentContact {
    
    public class createStudentParent {
        @InvocableVariable(label='studentfirstname')
        public string studentfname;
        @InvocableVariable(label='studentlastname')
        public string studentlname;
        @InvocableVariable(label='studentemail')
        public string studentemail;
        @InvocableVariable(label='studentphone')
        public string studentphone;

        @InvocableVariable(label='parentfirstname')
        public string parentfname;
        @InvocableVariable(label='parentlastname')
        public string parentlname;
        @InvocableVariable(label='parentemail')
        public string parentemail;
        @InvocableVariable(label='parentphone')
        public string parentphone;
        @InvocableVariable(label='parentrelationship')
        public string parentrelationship;
        @InvocableVariable(label='parent2firstname')
        public string parent2fname;
        @InvocableVariable(label='parent2lastname')
        public string parent2lname;
        @InvocableVariable(label='parent2email')
        public string parent2email;
        @InvocableVariable(label='parent2phone')
        public string parent2phone;
        @InvocableVariable(label='parent2relationship')
        public string parent2relationship;
        @InvocableVariable(label='graduationyear')
        public string graduationyear;
        @InvocableVariable(label='school')
        public string school;
        @InvocableVariable(label='city')
        public string city;
        @InvocableVariable(label='state')
        public string state;
        @InvocableVariable(label='postalcode')
        public string postalcode;
        @InvocableVariable(label='club')
        public string club;
        @InvocableVariable(label='ethnicity')
        public string ethnicity;
        @InvocableVariable(label='race')
        public string race;
        @InvocableVariable(label='infoconsent')
        public string infoconsent;
        @InvocableVariable(label='ifasian')
        public string ifasian;
        @InvocableVariable(label='ifmultiracial')
        public string ifmultiracial;
        @InvocableVariable(label='freelunch')
        public string freelunch;
        @InvocableVariable(label='sipstudent')
        public string sipstudent;
        @InvocableVariable(label='returningstudent')
        public string returningstudent;
        @InvocableVariable(label='birthdate')
        public date birthdate;
        @InvocableVariable(label='howdidyouhear')
        public string howdidyouhear;
        @InvocableVariable(label='howdidyouhearSM')
        public string howdidyouhearSM;
        @InvocableVariable(label='howdidyouhearAD')
        public string howdidyouhearAD;
        @InvocableVariable(label='howdidyouhearEVNT')
        public string howdidyouhearEVNT;

      
        
      }
    
     //Method to be invoked from the Process Builder flow
     @InvocableMethod(
       label='Student Parent Contact Creation'
       description='Create or Update Child-Parent Records')
    
     public static void createStudentParentContacts(List<createStudentParent> requests) {  
         List <contactLeadCheck.leadCheck> ldchks = new List <contactLeadCheck.leadCheck>();
         for (createStudentParent req : requests) {
             System.Debug('request='+req);
            
            //first check to see if the contact fname,lname,email already exists.  We don't want to dupe.
          
           Contact[] StudentID;
           Contact[] ParentID;
           Contact[] Parent2ID;
                     
           StudentID = [SELECT ID,mailingcity, mailingstate,mailingpostalcode,school__c, Returning_Club_student__c, mobilephone, npe01__PreferredPhone__c,
                               Race_Ethnicity__c, if_applicant_selected_asian__c, if_multiracial__c, Qualify_for_Free_Reduced_Lunch__c,
                               H_S_Graduation_Year__c, SIP_student__c, How_did_you_find_out_about_GWC_Clubs__c, birthdate,
                               How_did_you_find_out_Advertisement__c,How_did_you_find_out_Events__c,How_did_you_find_out_Social_Media__c
                                FROM Contact 
                        WHERE (email = :req.studentemail AND FirstName = :req.studentfname and LastName = :req.studentlname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
           ParentID = [SELECT ID, mailingcity, mailingstate,mailingpostalcode,mobilephone FROM Contact WHERE (email = :req.parentemail AND FirstName = :req.parentfname and LastName = :req.parentlname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
                                                     
           if(req.parent2fname != null && req.parent2lname != null){
                Parent2ID = [SELECT ID, mailingcity, mailingstate,mailingpostalcode,mobilephone FROM Contact WHERE (email = :req.parent2email AND FirstName = :req.parent2fname and LastName = :req.parent2lname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
           }
           String studentRecordtype = '';
           String parentRecordtype = '';
           boolean updateStudent = false;
            
             if(StudentID.size()>0){
               if(req.school != ''){
                StudentID[0].School__c = req.school;
                updateStudent = true;
               }
               if(req.city != ''){
                StudentID[0].mailingcity = req.city;
                updateStudent = true;
               }
               if(req.state != ''){
                StudentID[0].mailingstate = req.state;
                updateStudent = true;
               }
               if(req.race != ''){
                StudentID[0].Race_Ethnicity__c = req.race;
                updateStudent = true;
               }
               if(req.ifasian != ''){
                StudentID[0].if_applicant_selected_asian__c = req.ifasian;
                updateStudent = true;
               }
               if(req.ifmultiracial != ''){
                StudentID[0].if_multiracial__c = req.ifmultiracial;
                updateStudent = true;
               }
               if(req.freelunch != ''){
                StudentID[0].Qualify_for_Free_Reduced_Lunch__c = req.freelunch;
                updateStudent = true;
               }
               if(req.sipstudent != ''){
                StudentID[0].SIP_student__c = req.sipstudent;
                updateStudent = true;
               }
               if(req.returningstudent != ''){
                StudentID[0].Returning_Club_student__c = req.returningstudent;
                updateStudent = true;
               }
               if(req.graduationyear != ''){
                StudentID[0].H_S_Graduation_Year__c = req.graduationyear;
                updateStudent = true;
               }
               if(req.howdidyouhear != ''){
                StudentID[0].How_did_you_find_out_about_GWC_Clubs__c = req.howdidyouhear;
                updateStudent = true;
               }
    
               if(req.studentphone != ''){
                StudentID[0].MobilePhone = req.studentphone;
                StudentID[0].npe01__PreferredPhone__c = 'Mobile';
                updateStudent = true;
               }
               if(req.howdidyouhearAD != ''){
                StudentID[0].How_did_you_find_out_Advertisement__c = req.howdidyouhearAD;
                updateStudent = true;
               }
               if(req.howdidyouhearSM != ''){
                StudentID[0].How_did_you_find_out_Social_Media__c = req.howdidyouhearSM;
                updateStudent = true;              
               }
               if(req.birthdate != null){
                StudentID[0].birthdate = req.birthdate;
                updateStudent = true;
               }   
               if (updateStudent){
                   try{
                    update StudentID;
                   } catch(Dmlexception e){
                     System.debug('Exception Updating Student: ' + e.getMessage());
                   }
               }
            }
            else  //contact doesn't exist.  We'll insert a new one.
            {
                RecordType[] srecordtype = [SELECT ID FROM recordtype where SObjectType = 'Contact'
                                                     AND name = 'Student'];
               
               
                if (srecordtype.size() > 0) {studentRecordtype = srecordtype[0].Id;}
                
                
                    
                System.debug('In Else no Match');
                Contact iContact = new Contact();
                iContact.FirstName = req.studentfname;
                iContact.LastName = req.studentlname;
                iContact.email = req.studentemail;
                iContact.mailingcity = req.city;
                iContact.mailingstate = req.state;
                iContact.MailingPostalCode = req.postalcode;
                iContact.MobilePhone = req.studentphone;
                iContact.npe01__PreferredPhone__c = 'Mobile';
                iContact.Club_Enrollment_Student__c = true;
                iContact.Contact_Type__c = 'Student';
                iContact.RecordTypeID = studentRecordType;  ///Sandbox specific id
                iContact.npe01__Primary_Address_Type__c = 'Home';
                iContact.npe01__Preferred_Email__c = 'Personal';
                iContact.School__c = req.school;
                iContact.Race_Ethnicity__c = req.race;
                iContact.if_applicant_selected_asian__c = req.ifasian;
                iContact.If_Multiracial__c = req.ifmultiracial;
                iContact.Qualify_for_Free_Reduced_Lunch__c = req.freelunch;
                iContact.SIP_student__c = req.sipstudent;
                iContact.Returning_Club_student__c = req.returningstudent;
                iContact.H_S_Graduation_Year__c = req.graduationyear;
                iContact.How_did_you_find_out_about_GWC_Clubs__c = req.howdidyouhear;
                iContact.How_did_you_find_out_Social_Media__c= req.howdidyouhearSM;
                iContact.How_did_you_find_out_Advertisement__c = req.howdidyouhearAD;
                iContact.How_did_you_find_out_Events__c = req.howdidyouhearEVNT; 
                iContact.birthdate = req.birthdate;
           
                
                try{
                    insert iContact;
                   } catch(Dmlexception e){
                     System.debug('Exception Inserting Student: ' + e.getMessage());
                   }
                 contactLeadCheck.leadCheck ldchk = new contactLeadCheck.leadCheck();
                 ldchk.contactid = iContact.Id;
                 ldchks.add(ldchk);
                //for a new student, start the Canvas Create User process
             //   CanvasAPICalloutMethods.createStudentUser_CalloutFromFuture(req.studentfname, req.studentlname, req.studentfname, req.studentemail, req.studentemail);
            } //end else new insert
         
         
         //Parent Processing
         boolean updateParent = false;
            
             if(ParentID.size()>0){
               
               if(req.city != ''){
                ParentID[0].mailingcity = req.city;
                updateParent = true;
               }
               if(req.state != ''){
                ParentID[0].mailingstate = req.state;
                updateParent = true;
               }
               if(req.postalcode != ''){
                ParentID[0].mailingpostalcode = req.postalcode;
                updateParent = true;
               }
                if(req.parentphone != ''){
                ParentID[0].mobilephone = req.parentphone;
                updateParent = true;
               }
               if (updateParent){
                   try{
                    update ParentID;
                   } catch(Dmlexception e){
                     System.debug('Exception Updating Parent: ' + e.getMessage());
                   }
               }
            }
            else  //contact doesn't exist.  We'll insert a new one.
            {
                RecordType[] precordtype = [SELECT ID FROM recordtype where SObjectType = 'Contact'
                                                     AND name = 'General Contact'];
                if (precordtype.size() > 0) {parentRecordtype = precordtype[0].Id;}
                Contact iContact = new Contact();
                iContact.FirstName = req.parentfname;
                iContact.LastName = req.parentlname;
                iContact.email = req.parentemail;
                iContact.mailingcity = req.city;
                iContact.mailingstate = req.state;
                iContact.MailingPostalCode = req.postalcode;
                iContact.Club_Enrollment_Student__c = true;
                iContact.Contact_Type__c = 'Parent';
                iContact.RecordTypeID = parentRecordtype; 
                iContact.npe01__Primary_Address_Type__c = 'Home';
                iContact.npe01__Preferred_Email__c = 'Personal';
                iContact.MobilePhone = req.parentphone;
                iContact.npe01__PreferredPhone__c = 'Mobile';
                
                try{
                    insert iContact;
                   } catch(Dmlexception e){
                     System.debug('Exception Inserting Parent: ' + e.getMessage());
                   } 
                
                contactLeadCheck.leadCheck ldchk = new contactLeadCheck.leadCheck();
                ldchk.contactid = iContact.Id;
                ldchks.add(ldchk);
            }
            updateParent = false;
            if(Parent2ID.size()>0){
               
               if(req.city != ''){
                Parent2ID[0].mailingcity = req.city;
                updateParent = true;
               }
               if(req.state != ''){
                Parent2ID[0].mailingstate = req.state;
                updateParent = true;
               }
               if(req.postalcode != ''){
                Parent2ID[0].mailingpostalcode = req.postalcode;
                updateParent = true;
               }
               if(req.parent2phone != ''){
                Parent2ID[0].mobilephone = req.parent2phone;
                updateParent = true;
               }

               if (updateParent){
                   try{
                    update Parent2ID;
                   } catch(Dmlexception e){
                     System.debug('Exception Updating Parent: ' + e.getMessage());
                   }
               }
            }
            else  //contact doesn't exist.  We'll insert a new one.
            {
                if((String.isBlank(req.parent2lname) == false )&&(String.isBlank(req.parent2email)==false)){
                    RecordType[] precordtype = [SELECT ID FROM recordtype where SObjectType = 'Contact'
                                                     AND name = 'General Contact'];
                    if (precordtype.size() > 0) {parentRecordtype = precordtype[0].Id;}
                    Contact iContact = new Contact();
                    iContact.FirstName = req.parent2fname;
                    iContact.LastName = req.parent2lname;
                    iContact.email = req.parent2email;
                    iContact.mailingcity = req.city;
                    iContact.mailingstate = req.state;
                    iContact.MailingPostalCode = req.postalcode;
                    iContact.Club_Enrollment_Student__c = true;
                    iContact.Contact_Type__c = 'Parent';
                    iContact.RecordTypeID = parentRecordtype; 
                    iContact.npe01__Primary_Address_Type__c = 'Home';
                    iContact.npe01__Preferred_Email__c = 'Personal';
                    iContact.MobilePhone = req.parent2phone;
                    iContact.npe01__PreferredPhone__c = 'Mobile';
                
                    try{
                        insert iContact;
                       } catch(Dmlexception e){
                         System.debug('Exception Inserting Parent: ' + e.getMessage());
                       }  
                    contactLeadCheck.leadCheck ldchk = new contactLeadCheck.leadCheck();
                    ldchk.contactid = iContact.Id;
                    ldchks.add(ldchk);
                }
             }
         }
         if (ldchks.size() > 0) {
             contactLeadCheck.checkForMatchingLead(ldchks);
         }

     }


}