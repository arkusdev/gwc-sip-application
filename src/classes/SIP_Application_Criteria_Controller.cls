public with sharing class SIP_Application_Criteria_Controller {

	public List<SIP_Application_Criteria__c> records {get;private set;}
	public List<String> optionsForContact {get;private set;}
	public List<String> optionsForSIPApplication {get;private set;}
	public List<String> targets {get;private set;}
	public List<String> markets {get;private set;}

	public SIP_Application_Criteria_Controller(){
		records = [SELECT Id,Name,Target__c,Number_of_Spots__c,Active__c,Field_Name__c,Object_Name__c,Type__c,At_Least_Less_Than__c,Percent__c,Field_Values__c,Particular_Market__c,Priority__c FROM SIP_Application_Criteria__c ORDER BY Priority__c ASC];
		
		SObjectType contactType = Schema.getGlobalDescribe().get('Contact');
		Map<String,Schema.SObjectField> cfields = contactType.getDescribe().fields.getMap();
		optionsForContact = new List<String>(cfields.keySet());
		optionsForContact.sort();

		SObjectType sipType = Schema.getGlobalDescribe().get('SIP_Application__c');
		Map<String,Schema.SObjectField> sipfields = sipType.getDescribe().fields.getMap();
		optionsForSIPApplication = new List<String>(sipfields.keySet());
		optionsForSIPApplication.sort();

		targets = new List<String>();
		targets.add('');
		targets.add('National');
		targets.add('Market');
		targets.add('Classroom');

		markets = new List<String>();
		markets.add('');
		for(Metro_Area__c ma : [SELECT Name FROM Metro_Area__c ORDER BY Name ASC]){
			markets.add(ma.Name);
		}
		
	}

	@RemoteAction
	public static List<Response> saveCustomSettings(String recordsString){

		Savepoint sp = Database.setSavepoint();

		List<Response> response = new List<Response>();
		try{
			List<Object> listA = (List<Object>) JSON.deserializeUntyped(recordsString);

			List<SIP_Application_Criteria__c> records = new List<SIP_Application_Criteria__c>();
			Set<Id> ids = new Set<Id>();

			Integer i;
			for(i=0;i<listA.size();i++){
				Map<String, Object> m = (Map<String, Object>)listA[i];

				if(!String.isEmpty((String)m.get('id'))){
					ids.add((String)m.get('id'));
				}
				if(String.isEmpty((String)m.get('type'))){
					response.add( new Response(i,'You must to select a type.') );
					break;
				}
				//for equals
				if((String)m.get('type') == 'Equals'){
					if( String.isEmpty((String)m.get('name')) || String.isEmpty((String)m.get('fieldName')) || String.isEmpty((String)m.get('objectName')) || ((List<Object>)m.get('fieldValues')).size()==0 ){
						response.add( new Response(i,'Empty Required Fields.') );
						break;
					}else{

						if(validateFieldValues( (String)m.get('objectName') , (String)m.get('fieldName') , (List<Object>)m.get('fieldValues') )){
							SIP_Application_Criteria__c record = new SIP_Application_Criteria__c();
							record.Active__c = (Boolean)m.get('active');
							record.Name = (String)m.get('name');
							record.Field_Name__c = (String)m.get('fieldName');
							record.Object_Name__c = (String)m.get('objectName');
							record.Type__c = (String)m.get('type');
							List<Object> fieldValues = (List<Object>)m.get('fieldValues');
							Integer j;
							record.Field_Values__c = '';
							for(j=0;j<fieldValues.size();j++){
								record.Field_Values__c += ((Map<String, Object>)fieldValues[j]).get('name') + ';';
							}
							record.Priority__c = i;
							record.Particular_Market__c = (String)m.get('market');
							record.Target__c = (String)m.get('target');
							if( !String.isEmpty((String)m.get('id')) ){
								record.Id = (String)m.get('id');
							}
							records.add( record );

						}else{
							response.add( new Response(i,'Date format must be yyyy-mm-dd.') );
							break;
						}
					}
				}else if((String)m.get('type') == 'Percentage'){
					//for percentage
					if( String.isEmpty((String)m.get('name')) || String.isEmpty((String)m.get('fieldName')) || String.isEmpty((String)m.get('objectName'))
					|| String.isEmpty((String)m.get('operator')) || (String.isEmpty(String.valueOf(m.get('percentage'))) && String.isEmpty(String.valueOf(m.get('spots')))) || ((List<Object>)m.get('fieldValues')).size()==0 ){
						response.add( new Response(i,'Empty Required Fields.') );
						break;
					}else{

						if(validateFieldValues( (String)m.get('objectName'), (String)m.get('fieldName') , (List<Object>)m.get('fieldValues') )){

							SIP_Application_Criteria__c record = new SIP_Application_Criteria__c();
							record.Active__c = (Boolean)m.get('active');
							record.Name = (String)m.get('name');
							record.Field_Name__c = (String)m.get('fieldName');
							record.Object_Name__c = (String)m.get('objectName');
							record.Type__c = (String)m.get('type');
							record.At_Least_Less_Than__c = (String)m.get('operator');
							
							List<Object> fieldValues = (List<Object>)m.get('fieldValues');
							Integer j;
							record.Field_Values__c = '';
							for(j=0;j<fieldValues.size();j++){
								record.Field_Values__c += ((Map<String, Object>)fieldValues[j]).get('name') + ';';
							}
							record.Priority__c = i;
							record.Particular_Market__c = (String)m.get('market');
							record.Target__c = (String)m.get('target');
							if(!String.isEmpty((String)m.get('spots'))){
								record.Number_of_Spots__c = Integer.valueOf((String)m.get('spots'));
							}else if(!String.isEmpty((String)m.get('percentage'))){
								record.Percent__c = Decimal.valueOf((String)m.get('percentage'));
							}
							if( !String.isEmpty((String)m.get('id')) ){
								record.Id = (String)m.get('id');
							}
							records.add( record );

						}else{
							response.add( new Response(i,'Date format must be yyyy-mm-dd.') );
							break;
						}
					}
				}else if((String)m.get('type') == 'Location'){
					if(String.isEmpty((String)m.get('name'))){
						response.add( new Response(i,'Empty Required Fields.') );
						break;
					}else{
						SIP_Application_Criteria__c record = new SIP_Application_Criteria__c();
						record.Active__c = (Boolean)m.get('active');
						record.Name = (String)m.get('name');
						record.Type__c = (String)m.get('type');
						record.Target__c = (String)m.get('target');
						record.Priority__c = i;
						record.Particular_Market__c = (String)m.get('market');
						if( !String.isEmpty((String)m.get('id')) ){
							record.Id = (String)m.get('id');
						}
						records.add( record );	
					}
				}
			}

			if(response.size()==0){
				delete [SELECT Id FROM SIP_Application_Criteria__c WHERE ID NOT IN :ids];
				upsert records;
			}

		}catch(Exception e){
            Database.rollback( sp );
            response.add( new Response(-1, e.getMessage() + ' - line: ' + e.getLineNumber() ) );
        }

        return response;
	}

	private static Boolean validateFieldValues(String objectName, String fieldNameApi, List<Object> values){

		Schema.DescribeFieldResult f = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(fieldNameApi).getDescribe();

		if (f.getType() == Schema.DisplayType.Date){
			Integer j;
			String val;
			List<String> auxArray;
			for(j=0;j<values.size();j++){
				val = (String)((Map<String, Object>)values[j]).get('name');
				auxArray = val.split('-');
				if(auxArray.size()!=3){
					return false;
				}else{
					if( !auxArray[0].isNumeric() || !auxArray[1].isNumeric() || !auxArray[2].isNumeric() ){
						return false;
					}else{
						if( auxArray[0].length()!= 4 ){
							return false;
						}
						if( Integer.valueOf(auxArray[1]) > 12 || auxArray[1].length()!= 2 ){
							return false;
						}
						if( Integer.valueOf(auxArray[2]) > 31 || auxArray[2].length()!= 2 ){
							return false;
						}
					}
				}
			}
		}

		return true;

	}

	@RemoteAction
	public static List<Response> executeClassAssignment(String year){
		SIP.ClassAssignment(year);

		return new List<Response>();
	}

	public class Response{
		public Integer row {get;set;}
		public String errorMsg{get;set;}

		public Response(Integer r, String e){
			this.row = r;
			this.errorMsg = e;
		}

	}

}