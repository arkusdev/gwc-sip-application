global class SIPApplicationAssignmentBatch implements Database.Batchable<sObject> {
	String year;
	String query;
	Id programId;
	Boolean stillApplicants;
	Boolean runBatchAgain;
	SIP_Placeholder__c placeHolder;
	
	global SIPApplicationAssignmentBatch(String year, Id programId) {
		this.year = year;
		this.placeHolder = SIP_Placeholder__c.getOrgDefaults();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		this.query = 'select Name,'
							+ 'ApplicantAssignedCount__c,'
							+ 'Geolocation__latitude__s,'
							+ 'Geolocation__longitude__s,'
							+ 'SIP_Application_Sorted_List__c,'
							+ 'PriorityPercentageMap__c,'
							+ 'Metro_Area__c,'
							+ 'Metro_Area__r.Name,'
							+ 'Metro_Area__r.Region__c,'
							+ 'Metro_Area__r.Region__r.Name,'
							+ 'Metro_Area__r.Region__r.WaitlistedSpots__c'
						+ ' from SIP_Program__c'
						+ ' where Year_Active__c = \'' + this.year + '\''
						+ ' and Inactive__c = false'
						+ ' and Metro_Area__c != null';
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		SIP.CLASS_CAPACITY 			= 20;
		SIP.WAITLISTED 				= 15;
		SIP.APPLICANT_LIMIT			= 500;
		Map<Id, SIP_Program__c> programMap = new Map<Id, SIP_Program__c>((List<SIP_Program__c>) scope);
		Set<String> idsSet = new Set<String>();
		Map<Id, List<SIP_Applicant>> programToSortedApplicantListMap = new Map<Id, List<SIP_Applicant>>();
		SIP.PROGRAM_PERCENTAGE_MAP = new Map<Id, SIP.ProgramPercentage>();
		List<Id> regionIds = new List<Id>();
		
		for (SIP_Program__c program : programMap.values()) {
			programToSortedApplicantListMap.put(program.Id, new List<SIP_Applicant>());
			SIP.PROGRAM_PERCENTAGE_MAP.put(program.Id, new SIP.ProgramPercentage());
			regionIds.add(program.Metro_Area__r.Region__c);
			
			if (!String.isBlank(program.SIP_Application_Sorted_List__c)) 
				idsSet.addAll(program.SIP_Application_Sorted_List__c.split(';'));
			
			if (!String.isBlank(program.PriorityPercentageMap__c)) {
				String[] priorityCountList = program.PriorityPercentageMap__c.split(';');
				if (priorityCountList != null) {
					for (String priorityCount : priorityCountList) {
						String[] splitted = priorityCount.split(':');
						SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToNumber.put(Integer.valueOf(splitted[0]), Integer.valueOf(splitted[1]));
						Integer percentage = program.ApplicantAssignedCount__c < SIP.CLASS_CAPACITY ? (SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToNumber.get(Integer.valueOf(splitted[0])) * 100) / SIP.CLASS_CAPACITY : (SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToNumber.get(Integer.valueOf(splitted[0])) * 100) / (SIP.CLASS_CAPACITY + SIP.WAITLISTED);
						SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToPercentage.put(Integer.valueOf(splitted[0]), percentage);
					}
				}
			}
		}
		Map<Id, Region__c> regionMap = new Map<Id, Region__c>([select Name, WaitlistedSpots__c from Region__c where Id IN: regionIds]);

		SIP.CRITERIA_LIST = [select At_Least_Less_Than__c,
								Field_Name__c,
								Field_Values__c,
								Object_Name__c,
								Particular_Market__c,
								Percent__c,
								Type__c,
								Priority__c,
								Target__c
							from SIP_Application_Criteria__c
							where Active__c = true
							order by Priority__c ASC];				
		Set<String> applicationFields = new Set<String>();
		Set<String> contactFields = new Set<String>();
		
		//Getting the fields needed for the query based on the different Field_Name__c in the SIP_Application_Criteria__c records
		//Criteria rules are always based on 2 objects "SIP_Application_Criteria__c" & "Contact"
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if (criteria.Object_Name__c == 'SIP_Application__c')
				applicationFields.add(criteria.Field_Name__c);
			if (criteria.Object_Name__c == 'Contact')
				contactFields.add(criteria.Field_Name__c);
		}
		
		String applicationQuery = 'select Applicant__c,'
										+ 'Acceptance_Status__c,'
										+ 'Acceptance_Status_Determination__c,'
										+ 'Category__c,'
										+ 'Updated_by_Placement_Algorithm__c'; 
		if (!applicationFields.isEmpty())										
			applicationQuery += ',' + String.join(new List<String>(applicationFields), ',') ;
		applicationQuery += ' from SIP_Application__c'
								+ ' where Id IN (\'' + String.join(new List<String>(idsSet), '\',\'') + '\')';
		SIP.APPLICATION_MAP = new Map<Id, SIP_Application__c>((List<SIP_Application__c>)Database.query(applicationQuery));
		
		//getting all the Contacts from the application records
		Map<Id, Boolean> applicantAssignedMap = new Map<Id, Boolean>();//For knowing if application already has a classroom assigned
		List<Id> contactIds = new List<Id>();
		for (SIP_Application__c application : SIP.APPLICATION_MAP.values()) {
			contactIds.add(application.Applicant__c);
			applicantAssignedMap.put(application.Id, false);
		}

		this.runBatchAgain = false;
		for (SIP_Program__c program : programMap.values()) {
			Integer i = 0;
			if (!String.isBlank(program.SIP_Application_Sorted_List__c)) {
				String[] sortedApps = program.SIP_Application_Sorted_List__c.split(';');
				while (i < SIP.APPLICANT_LIMIT && sortedApps.size() > i) {
					programToSortedApplicantListMap.get(program.Id).add(new SIP_Applicant(SIP.APPLICATION_MAP.get(sortedApps[i])));
					i++;
				}
				if (sortedApps.size() > SIP.APPLICANT_LIMIT) {
					this.runBatchAgain = true;
					/*List<String> newApplicantList = new List<String>();
					for (Integer j = 20; j < sortedApps.size(); j++)
						newApplicantList.add(sortedApps[j]);
					program.SIP_Application_Sorted_List__c = String.join(newApplicantList, ';');*/
				}
			}
		}
		String contactQuery = 'select MailingLatitude,'
									+ 'MailingLongitude';
		if (!contactFields.isEmpty())										
			contactQuery += ',' + String.join(new List<String>(contactFields), ',') ;
		contactQuery += ' from Contact' 
						+ ' where Id IN (\'' + String.join(contactIds, '\',\'') + '\')';
		SIP.CONTACT_MAP = new Map<Id, Contact>((List<Contact>)Database.query(contactQuery));

		this.stillApplicants = true;//For knowing if there are applicants that still need to be assigned
		Boolean startingId = String.isBlank(this.placeHolder.ProgramId__c) ? true : false;
		//if we still have applicants that were not assigned we keep iterating
		Boolean breakLoop = false;
		while (this.stillApplicants)	{
			this.stillApplicants = false;

			for (SIP_Program__c program : programMap.values()) {
				if (startingId) {
					//checking if the classroom is full
					if (program.ApplicantAssignedCount__c < (SIP.CLASS_CAPACITY + SIP.WAITLISTED)) {
						
						Integer applicationCount = 0;//for knowing how many applicants took us to find one to assigned to the market, so later we can remove them
						for (SIP_Applicant application : programToSortedApplicantListMap.get(program.Id)) {
							if (!applicantAssignedMap.get(application.applicant.Id) && String.isBlank(application.applicant.Acceptance_Status_Determination__c) && String.isBlank(application.applicant.Category__c)) {
								applicantAssignedMap.put(application.applicant.Id, true);
								//if we reached the class capacity we need to waitlist the application
								if (program.ApplicantAssignedCount__c < SIP.CLASS_CAPACITY) {
									SIP.APPLICATION_MAP.get(application.applicant.Id).Category__c = program.Name;
									SIP.APPLICATION_MAP.get(application.applicant.Id).Acceptance_Status__c = 'Classroom Assignments - Onboarding Documents';
									SIP.APPLICATION_MAP.get(application.applicant.Id).Updated_by_Placement_Algorithm__c = Datetime.now();
									program.ApplicantAssignedCount__c++;
									this.stillApplicants = true;
								} else if (regionMap.get(program.Metro_Area__r.Region__c).WaitlistedSpots__c > 0){
									SIP.APPLICATION_MAP.get(application.applicant.Id).Category__c = 'Wait-listed: ' + program.Metro_Area__r.Region__r.Name;
									SIP.APPLICATION_MAP.get(application.applicant.Id).Acceptance_Status__c = 'Waitlisted';
									SIP.APPLICATION_MAP.get(application.applicant.Id).Updated_by_Placement_Algorithm__c = Datetime.now();
									regionMap.get(program.Metro_Area__r.Region__c).WaitlistedSpots__c--;
									this.stillApplicants = true;
								}
								applicationCount++;
								//we re-calculate the percentage of each rule for the program
								SIP.programCriteriaPercentageCalc(program, SIP.APPLICATION_MAP.get(application.applicant.Id));
								break;//Every program grabs one each turn, this is why we break
							}
						}
						
						//removing all the already assigned applicants
						for (Integer i = applicationCount - 1; i >= 0; i--) {
							programToSortedApplicantListMap.get(program.Id).remove(i);
							/*List<Id> applicantIds = new List<Id>();
							for (SIP_Applicant applicant : programToSortedApplicantListMap.get(program.Id))
								applicantIds.add(applicant.applicant.Id);
							program.SIP_Application_Sorted_List__c = String.join(applicantIds, ';');*/
						}
						
						if (this.stillApplicants && SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).percentageReached) {
							//we sort again for the classroom because the percentage changed
							SIP.CURRENT_PROGRAM = program;
							breakLoop = true;
							SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).percentageReached = false;
							break;
						}
					}
				} else {
					if (program.Id == this.placeHolder.ProgramId__c) {
						startingId = true;
						this.stillApplicants = true;
					}
				}
			}
			if (breakLoop)
				break;
		}

		if (!this.stillApplicants && !this.runBatchAgain) {
			for (SIP_Application__c application : SIP.APPLICATION_MAP.values()) {
				if (!applicantAssignedMap.get(application.Id) && String.isBlank(application.Category__c)) {
					application.Acceptance_Status__c = 'Declined';
				}
			}
		} else {
			for (SIP_Program__c program : programMap.values()) {
				List<String> priorityPercentageList = new List<String>();
				for (Integer key : SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToNumber.keySet())
					priorityPercentageList.add(key + ':' + SIP.PROGRAM_PERCENTAGE_MAP.get(program.Id).criteriaPriorityToNumber.get(key));
				program.PriorityPercentageMap__c = String.join(priorityPercentageList, ';');
				if (!String.isBlank(program.SIP_Application_Sorted_List__c)) {
					List<String> idList = new List<String>();
					for (String splittedId : program.SIP_Application_Sorted_List__c.split(';')) {
						if (String.isBlank(SIP.APPLICATION_MAP.get(splittedId).Category__c) && !applicantAssignedMap.get(splittedId)) {
							idList.add(splittedId);
						}
					}
					program.SIP_Application_Sorted_List__c = String.join(idList, ';');
				}
			}

		}
		update SIP.APPLICATION_MAP.values();
		update programMap.values();
		update regionMap.values();
		if (this.stillApplicants) {
			this.programId = SIP.CURRENT_PROGRAM.Id;
		}
		this.placeHolder.ProgramId__c = this.programId;
		this.placeHolder.StillApplicants__c = this.stillApplicants;
		this.placeHolder.RunBatchAgain__c = this.runBatchAgain;
		this.placeHolder.Year__c = this.year;
		upsert this.placeHolder;
	}
	
	global void finish(Database.BatchableContext BC) {
		this.placeHolder = SIP_Placeholder__c.getOrgDefaults();
		if (this.placeHolder.StillApplicants__c && !Test.isRunningTest()) {
			Database.executeBatch(new SIPApplicationSortByProgramBatch(this.placeHolder.Year__c, this.placeHolder.ProgramId__c), 1);
		} else {
			if (this.placeHolder.RunBatchAgain__c && !Test.isRunningTest()) {
				Database.executeBatch(new SIPApplicationAssignmentBatch(this.placeHolder.Year__c, null));
			} else {
				
				List<SIP_Application__c> applicationList = [select Acceptance_Status__c from SIP_Application__c where Category__c = null and Acceptance_Status__c = null and Application_Year__c =: this.placeHolder.Year__c and SIP_Stage__c = 'Holding Bucket'];
				for (SIP_Application__c app : applicationList)
					app.Acceptance_Status__c = 'Declined';
				update applicationList;
				this.placeHolder.ProgramId__c = null;
				this.placeHolder.StillApplicants__c = false;
				this.placeHolder.RunBatchAgain__c = false;
				this.placeHolder.Year__c = null;
				upsert placeHolder;
			}
		}
	}
	
}