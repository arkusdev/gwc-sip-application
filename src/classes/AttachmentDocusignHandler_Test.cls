@IsTest
public class AttachmentDocusignHandler_Test {
    
    
    static TestMethod void CreateAttachmentTest() { 
       
        dsfs__DocuSign_Status__c DSS = new dsfs__DocuSign_Status__c();
        DSS.dsfs__Sender__c = 'Test Sender';
        DSS.dsfs__Sender_Email__c = 'tst@lslslsl.com';
        DSS.dsfs__Envelope_Status__c = 'Completed';
        DSS.dsfs__Subject__c = 'Test Subject';
        insert DSS;
        
        dsfs__DocuSign_Recipient_Status__c recip = new dsfs__DocuSign_Recipient_Status__c();
        recip.dsfs__DocuSign_Recipient_Email__c = 'tst@lslslsl.com';
        recip.dsfs__Parent_Status_Record__c = DSS.Id;
        recip.name = 'Test Receiver';
        recip.dsfs__DocuSign_Recipient_Id__c = '4ljflalkf';
        insert recip;
        
        Contact parent = new Contact();
        parent.firstname = 'Test';
        parent.LastName = 'Receiver';
        parent.Email = 'tst@lslslsl.com';
        insert parent;
        
        Blob bodyBlob = Blob.valueOf('Unit test attachment body');
        Attachment attch = new Attachment();
        attch.parentId = DSS.Id;
        attch.Name = 'Parental Release Form-No Object.pdf';
        attch.ContentType = 'application/pdf';
        attch.Body = bodyBlob;
        attch.IsPrivate = False;
        
        insert attch;
        
        
    }
        


}