@isTest
public class ClubEnrollmentControllerTest {

  static testMethod void ClubEnrollmentController() {
        //create an organization for the club
       Account acct = new Account();
       acct.name = 'Club Host';
       insert acct;
       //create the Club object
       Club__c clubObjIns = new Club__c();
               
        clubObjIns.Host_Organization__c = acct.id;
        clubObjIns.Club_Name__c = 'Dummy Club';
        clubObjIns.Meeting_Day__c = 'Sunday';        
        clubObjIns.Girls_In_Club__c = 23;        
        clubObjIns.Launch_Date__c = Date.today();        
        clubObjIns.Club_Start_Time__c = '4:30 PM';
        clubObjIns.Club_End_Time__c = '6:30 PM';
       
        insert clubObjIns;  
        PageReference pageRef = Page.PreLaunchCheckList;
        ApexPages.currentPage().getParameters().put('clubid', clubObjIns.name);
       // Club_Student_Enrollment_POC__c csenroll = new ClubStudentEnrollmentFormController();
        ClubStudentEnrollmentFormController  controller = new ClubStudentEnrollmentFormController ();
        Club_Student_Enrollment_POC__c csenroll = controller.clubStudentEnrollmentForm();
        Club_Student_Enrollment_POC__c csenroll2 = controller.clubObj;

      Test.startTest();
      //  controller.
        csenroll.Student_First_Name__c = 'Quentin';
        csenroll.Student_Last_Name__c = 'Crisp';
        csenroll.Student_Email__c = 'oeoweiwr@lslsls.com';
        csenroll.Parent_First_Name__c = 'Quentin';
        csenroll.Parent_Last_Name__c = 'Crisp';
        csenroll.Parent_Email__c = 'weiwr@lsls.com';

        csenroll.City__c = 'Detroit';
        csenroll.State__c = 'MI';
        csenroll.Club_id__c = clubObjIns.name;
        csenroll.Club_Name__c = clubObjIns.Name;
        csenroll.Data_Collection_Consent__c = true;
        csenroll.Ethnicity__c = 'Not Hispanic or Latino';
        csenroll.Student_Expected_High_School_Graduation__c = '2018';
        csenroll.Promotional_Material_Consent__c = true;
        csenroll.Race__c = 'White';
        csenroll.Returning_Club_Student__c = 'No';
        csenroll.Same_Email__c = false;
        csenroll.Student_Under_13__c = true;
        controller.save();
      
      
    
        controller.showFailedPopup();
        controller.closeFailedPopup();
        controller.showPopup();
        controller.closePopup();
        controller.getClubName();
        controller.Reset();
        controller.getItems();
       
      Test.stopTest();
      //System.assertNotEquals(null, nextPage);
  }    

    
  static testMethod void ClubEnrollmentController2() {
        //create an organization for the club
       Account acct = new Account();
       acct.name = 'Club Host';
       insert acct;
       //create the Club object
       Club__c clubObjIns = new Club__c();
               
        clubObjIns.Host_Organization__c = acct.id;
        clubObjIns.Club_Name__c = 'Dummy Club';
        clubObjIns.Meeting_Day__c = 'Sunday';        
        clubObjIns.Girls_In_Club__c = 23;        
        clubObjIns.Launch_Date__c = Date.today();        
        clubObjIns.Club_Start_Time__c = '4:30 PM';
        clubObjIns.Club_End_Time__c = '6:30 PM';
       
        insert clubObjIns;  
        PageReference pageRef = Page.PreLaunchCheckList;
      //  ApexPages.currentPage().getParameters().put('clubid', clubObjIns.name);
       // Club_Student_Enrollment_POC__c csenroll = new ClubStudentEnrollmentFormController();
        ClubStudentEnrollmentFormController  controller = new ClubStudentEnrollmentFormController ();
        Club_Student_Enrollment_POC__c csenroll = controller.clubStudentEnrollmentForm();
        Club_Student_Enrollment_POC__c csenroll2 = controller.clubObj;

      Test.startTest();
      //  controller.
        csenroll.Student_First_Name__c = 'Quentin';
        csenroll.Student_Last_Name__c = 'Crisp';
        csenroll.Student_Email__c = 'oeoweiwr@lslsls.com';
        csenroll.Parent_First_Name__c = 'Quentin';
        csenroll.Parent_Last_Name__c = 'Crisp';
        csenroll.Parent_Email__c = 'weiwr@lsls.com';

        csenroll.City__c = 'Detroit';
        csenroll.State__c = 'MI';
        csenroll.Club_id__c = clubObjIns.name;
        csenroll.Club_Name__c = clubObjIns.name;
        csenroll.Data_Collection_Consent__c = true;
        csenroll.Ethnicity__c = 'Not Hispanic or Latino';
        csenroll.Student_Expected_High_School_Graduation__c = '2018';
        csenroll.Promotional_Material_Consent__c = true;
        csenroll.Race__c = 'White';
        csenroll.Returning_Club_Student__c = 'No';
        csenroll.Same_Email__c = false;
        csenroll.Student_Under_13__c = false;
        controller.save();
      
      
    
        controller.showFailedPopup();
        controller.closeFailedPopup();
        controller.showPopup();
        controller.closePopup();
        controller.getClubName();
        controller.Reset();
       
      Test.stopTest();
      //System.assertNotEquals(null, nextPage);
  }    


}