public with sharing class checkForLead {
    
    public static void checkingLeads(id contactid){
        List<Lead> leadtobedeleted = new List <Lead>();
        List<Contact> contactsToUpdate = new List <Contact>();
        List<Contact> cons = new List <Contact>();
        
        cons = [SELECT ID,AccountId, firstname, lastname,email,Coding_Experience__c, Computer_Interests__c,Region_of_Interest__c,
                           Daily_access_to_tech_at_home__c, Employer_Name__c,Interest_Type__c,TeacherInterest_Current_Role__c,Student_Grade_Level__c,
                           Family_who_works_in_tech__c, H_S_Graduation_Year__c, School__c,
                           How_are_you_connected_to_the_host_site__c,How_did_you_find_out_Other__c,How_did_you_find_out_about_GWC_Clubs__c,
                           Interest_Form_Contact_Type__c,If_Applicant_Selected_Asian__c,If_Multiracial__c, InterestFormSubmitDate__c ,
                           Club_Vol_Occupation__c,Job_Title__c,Partnership_Region_of_Interest__c, Partner_Working_Relationship__c,
                           Partner_Working_Relationship_Other__c, Race_Ethnicity__c, Teaching_Experience__c,Current_Position__c,
                           Teaching_Position__c,Type_of_Club_Location__c,Type_of_Donor_Support__c,US_Presence__c,What_does_your_company_do__c,
                           What_is_your_target_audience__c, Where_will_the_club_be_hosted__c,Race_ethnicity_additional_detail__c,
                           How_did_you_find_out_Social_Media__c,How_did_you_find_out_Advertisement__c,How_did_you_find_out_Events__c
                            FROM Contact where id = :contactid];
        
            
            
            if(cons.size() > 0){
            for (Contact con: cons){
                    Lead[] leadmatch;
                    System.debug('In LeadCheck '+ con.FirstName +'  ' + con.LastName + ' email '+con.Email);
                    leadmatch = [SELECT ID,Coding_Experience__c, Computer_Interests__c,Region_of_Interest__c,
                           Daily_access_to_tech_at_home__c, Employer_Name__c,Interest_Type__c,TeacherInterest_Current_Role__c,Student_Grade_Level__c,
                           Ethnicity_additional_information__c, Family_who_works_in_tech__c, Expected_H_S_Graduation_Year__c, School__c,
                           How_are_you_connected_to_the_host_site__c,How_did_you_find_out_Other__c,How_did_you_hear_about_GWC__c,
                           Interest_Form_Contact_Type__c,If_Applicant_Selected_Asian__c,If_Multiracial__c, InterestFormSubmitDate__c ,
                           Club_Vol_Occupation__c,Job_Title__c,Partner_Region_of_Interest__c, Partner_Type__c,Partner_Working_Relationship__c,
                           Partner_Working_Relationship_Other__c, Race_Ethnicity__c, Teaching_Experience__c,Current_Position__c,
                           Teaching_Position__c,Type_of_Club_Location__c,Type_of_Donor_Support__c,US_Presence__c,What_does_your_company_do__c,
                           What_is_your_target_audience__c, Where_will_the_club_be_hosted__c,How_did_you_find_out_Social_Media__c ,
                           How_did_you_find_out_Advertisement__c, How_did_you_find_out_Events__c ,Job_Organization__c 
                        FROM Lead 
                           WHERE (email = :con.email AND FirstName = :con.firstname and LastName = :con.LastName AND IsConverted = false
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        
                     if(leadmatch.size() > 0){
                       for(Lead ld: leadmatch){
                    
                           // System.debug('Org: '+con.AccountId );
                            System.debug('Match Found');
                            if(con.Coding_Experience__c == '')
                            	con.Coding_Experience__c = ld.Coding_Experience__c;
                            if(con.Computer_Interests__c == '')
                                con.Computer_Interests__c = ld.Computer_Interests__c;
                            if(con.Computer_Interests__c == '')
                                con.Daily_access_to_tech_at_home__c = ld.Daily_access_to_tech_at_home__c;
                            if(con.Race_Ethnicity__c == '')
                                con.Race_Ethnicity__c = ld.Race_Ethnicity__c;
                            if(con.If_Multiracial__C == '')
                                con.If_Multiracial__C = ld.If_Multiracial__c;
                            if(con.Race_ethnicity_additional_detail__c == '')
                                con.Race_ethnicity_additional_detail__c = ld.Ethnicity_additional_information__c;
                            if(con.H_S_Graduation_Year__c == '')
                                con.H_S_Graduation_Year__c = ld.Expected_H_S_Graduation_Year__c;
                            if(con.How_did_you_find_out_about_GWC_Clubs__c == '')
                                con.How_did_you_find_out_about_GWC_Clubs__c =  ld.How_did_you_hear_about_GWC__c;
                         // if(con.How_did_you_find_out_Original__c == '')
                            con.How_did_you_find_out_Original__c =  ld.How_did_you_hear_about_GWC__c;
                            if(con.Student_Grade_Level__c == '')
                                con.Student_Grade_Level__c = ld.Student_Grade_Level__c;
                    
                            con.Employer_Name__c = ld.Employer_Name__c;
                   
                            con.Interest_Type__c = ld.Interest_Type__c;
                    
                            con.TeacherInterest_Current_Role__c = ld.TeacherInterest_Current_Role__c;
                    
                            con.Race_ethnicity_additional_detail__c = ld.Ethnicity_additional_information__c;
                    
                            con.Family_who_works_in_tech__c = ld.Family_who_works_in_tech__c;
                   
                   
                            con.School__c = ld.School__c;
                    
                            con.How_are_you_connected_to_the_host_site__c = ld.How_are_you_connected_to_the_host_site__c;
                    
                            con.How_did_you_find_out_Other__c = ld.How_did_you_find_out_Other__c;
                     
                            con.Interest_Form_Contact_Type__c = ld.Interest_Form_Contact_Type__c;
                   
                            con.If_Applicant_Selected_Asian__c = ld.If_Applicant_Selected_Asian__c;
                    
                            con.InterestFormSubmitDate__c = ld.InterestFormSubmitDate__c;
                    
                            con.Job_Title__c = ld.Job_Title__c;
                   
                            con.Partnership_Region_of_Interest__c = ld.Partner_Region_of_Interest__c;
                   
                            con.Club_Vol_Occupation__c = ld.Club_Vol_Occupation__c;
                            //con.Partner_Working_Relationship_Other__c =  ld.Partnership_Additional_Information__c;
                            con.Partner_Working_Relationship__c = ld.Partner_Working_Relationship__c;
                    
                            con.Partner_Working_Relationship_Other__c = ld.Partner_Working_Relationship_Other__c;
                                    
                            con.Region_Of_Interest__c = ld.Region_of_Interest__c;
                    
                            con.Teaching_Experience__c = ld.Teaching_Experience__c;
                            con.Current_Position__c = ld.Current_Position__c;
                            con.Teaching_Position__c = ld.Teaching_Position__c;
                            con.Type_of_Club_Location__c = ld.Type_of_Club_Location__c;
                            con.Type_of_Donor_Support__c = ld.Type_of_Donor_Support__c;
                            con.US_Presence__c = ld.US_Presence__c;
                            //ld.website;
                            con.What_does_your_company_do__c = ld.What_does_your_company_do__c;
                            con.What_is_your_target_audience__c = ld.What_is_your_target_audience__c;
                            con.Club_Host_Location__c = ld.Where_will_the_club_be_hosted__c;
                            
                            //Below are fields added to the 3.0 Interest Form
                            con.How_did_you_find_out_Social_Media__c = ld.How_did_you_find_out_Social_Media__c;
                            con.How_did_you_find_out_Advertisement__c = ld.How_did_you_find_out_Advertisement__c;
                            con.How_did_you_find_out_Events__c = ld.How_did_you_find_out_Events__c;
							con.Job_Organization__c = ld.Job_Organization__c;
                            Database.LeadConvert lc = new Database.LeadConvert();
                            lc.setLeadId(ld.id);

                            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
                            lc.setConvertedStatus(convertStatus.MasterLabel);
                            lc.setContactId(con.id);
                            lc.setAccountId(con.accountId);
                            System.Debug('The Contact ID '+ con.id);
                            lc.setDoNotCreateOpportunity(TRUE);
                            Database.LeadConvertResult lcr = Database.convertLead(lc);
                    //  System.assert(lcr.isSuccess());
                    
                            //leadtobedeleted.add(ld);
                    
                            contactsToUpdate.add(con);
                    }
                }
                
            else
                System.debug('No Match Found');
            
            if(contactsToUpdate.size() > 0){
                    update contactsToUpdate;
            }
        }
     }  
   }
}