@IsTest
private class CanvasEnrollStudentPOC_Test {

    static testMethod void CreateStudentTest() {    
        Test.setMock(HttpCalloutMock.class, new CanvasAPICallout_MockMethods());
        List<CanvasEnrollStudentPOC.createStudentUser> studentUserList = new List<CanvasEnrollStudentPOC.createStudentUser>();
         CanvasEnrollStudentPOC.createStudentUser studentUser = new CanvasEnrollStudentPOC.createStudentUser();

        studentUser.fname = 'Sarah';
        studentUser.lname = 'Simpson';
        studentUser.short_name  = 'ssimpson';
        studentUser.unique_id  = 'ss@example.com';
        studentUserList.add(studentUser);
       
       try{
               for (CanvasEnrollStudentPOC.createStudentUser user : studentUserList) {                    
                      CanvasEnrollStudentPOC.createStudentUser(studentUserList);                                       
               }
           }
       catch(Exception e) {
          System.debug('Canvas Student Enroll Error:' + e);
            }     
    }
}