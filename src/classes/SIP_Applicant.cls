global class SIP_Applicant implements Comparable {
	
	public SIP_Application__c applicant;

	public SIP_Applicant(SIP_Application__c applicant) {
		this.applicant 	= applicant;
	}

	global Integer compareTo(Object compareTo) {
		Integer comparation = 0;
		//we compare every applicant against every applicant based on each criteria rule
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if (String.isBlank(criteria.Particular_Market__c) || SIP.CURRENT_PROGRAM.Metro_Area__r.Name == criteria.Particular_Market__c) {
				if (criteria.Type__c == 'Percentage') {
					comparation = this.compareToPercentageCriteria(criteria, compareTo);
				} else if (criteria.Type__c == 'Location') {
					comparation = this.compareToLocationCriteria(criteria, compareTo);
				} else if (criteria.Type__c == 'Equals') {
					comparation = this.compareToEqualsCriteria(criteria, compareTo);
				}
			}
			//checks if found already a sort order for the 2 records being compare
			if (comparation != 0) 
				break;
		}
		return comparation;
	}

	private Integer compareToPercentageCriteria(SIP_Application_Criteria__c criteria, Object compareTo) {
		Integer comparation = 0;
		if (criteria.At_Least_Less_Than__c != 'At Least' || SIP.PROGRAM_PERCENTAGE.criteriaPriorityToPercentage.get(Integer.valueOf(criteria.Priority__c)) == null || SIP.PROGRAM_PERCENTAGE.criteriaPriorityToPercentage.get(Integer.valueOf(criteria.Priority__c)) < Integer.valueOf(criteria.Percent__c)) {
			String[] applicantSplittedValues;
			String[] compareToSplittedValues;
			if (criteria.Object_Name__c == 'SIP_Application__c') {
				applicantSplittedValues = String.isBlank(String.valueOf(this.applicant.get(criteria.Field_Name__c))) ? null : String.valueOf(this.applicant.get(criteria.Field_Name__c)).split(';');
				compareToSplittedValues = String.isBlank(String.valueOf(((SIP_Applicant)compareTo).applicant.get(criteria.Field_Name__c))) ? null : String.valueOf(((SIP_Applicant)compareTo).applicant.get(criteria.Field_Name__c)).split(';'); 
			} else if (criteria.Object_Name__c == 'Contact') {
				applicantSplittedValues = String.isBlank(String.valueOf(SIP.CONTACT_MAP.get(this.applicant.Applicant__c).get(criteria.Field_Name__c))) ? null : String.valueOf(SIP.CONTACT_MAP.get(this.applicant.Applicant__c).get(criteria.Field_Name__c)).split(';');
				compareToSplittedValues = String.isBlank(String.valueOf(SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).get(criteria.Field_Name__c))) ? null : String.valueOf(SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).get(criteria.Field_Name__c)).split(';'); 
			}
			Boolean applicantContains = false;
			Boolean compareToContains = false;
			if (applicantSplittedValues != null) {
				for (String value : applicantSplittedValues) {
					if (!String.isBlank(value) && criteria.Field_Values__c.contains(value))
						applicantContains = true;
				}
			}
			if (compareToSplittedValues != null) {
				for (String value : compareToSplittedValues) {
					if (!String.isBlank(value) && criteria.Field_Values__c.contains(value))
						compareToContains = true;
				}
			}
			if (applicantContains && !compareToContains) {
				if (criteria.At_Least_Less_Than__c == 'At Least') {
					comparation = -1;
				} else {
					comparation = 1;	
				}
			} else if (!applicantContains && compareToContains) {
				if (criteria.At_Least_Less_Than__c == 'At Least') {
					comparation = 1;
				} else {
					comparation = -1;
				}
			}
		}
		return comparation;
	}

	private Integer compareToLocationCriteria(SIP_Application_Criteria__c criteria, Object compareTo) {
		Integer comparation = 0;
		if (SIP.CONTACT_MAP.get(this.applicant.Applicant__c).MailingLatitude != null 
			&& SIP.CONTACT_MAP.get(this.applicant.Applicant__c).MailingLongitude != null
			&& SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).MailingLatitude != null
			&& SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).MailingLongitude != null
			&& SIP.CURRENT_PROGRAM.Geolocation__latitude__s != null
			&& SIP.CURRENT_PROGRAM.Geolocation__longitude__s != null) {

			Location programLocation = Location.newInstance(SIP.CURRENT_PROGRAM.Geolocation__latitude__s, SIP.CURRENT_PROGRAM.Geolocation__longitude__s);
			Decimal applicantProgramDistance = programLocation.getDistance(Location.newInstance(SIP.CONTACT_MAP.get(this.applicant.Applicant__c).MailingLatitude, SIP.CONTACT_MAP.get(this.applicant.Applicant__c).MailingLongitude), 'mi');
			Decimal compareToProgramDistance = programLocation.getDistance(Location.newInstance(SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).MailingLatitude, SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).MailingLongitude), 'mi');
			if (applicantProgramDistance < compareToProgramDistance) {
				comparation = -1;
			} else if (applicantProgramDistance > compareToProgramDistance) {
				comparation = 1;
			}
		}
		return comparation;
	}

	private Integer compareToEqualsCriteria(SIP_Application_Criteria__c criteria, Object compareTo) {
		Integer comparation = 0;
		String applicantValue;
		String compareToValue;
		if (criteria.Object_Name__c == 'SIP_Application__c') {
			applicantValue = String.valueOf(this.applicant.get(criteria.Field_Name__c));
			compareToValue = String.valueof(((SIP_Applicant)compareTo).applicant.get(criteria.Field_Name__c)); 
		} else if (criteria.Object_Name__c == 'Contact') {
			applicantValue = String.valueOf(SIP.CONTACT_MAP.get(this.applicant.Applicant__c).get(criteria.Field_Name__c));
			compareToValue = String.valueOf(SIP.CONTACT_MAP.get(((SIP_Applicant)compareTo).applicant.Applicant__c).get(criteria.Field_Name__c)); 
		}
		if (!String.isBlank(applicantValue) && criteria.Field_Values__c.contains(applicantValue) && (String.isBlank(compareToValue) || !criteria.Field_Values__c.contains(compareToValue))) {
			comparation = -1;
		} else if ((String.isBlank(applicantValue) || !criteria.Field_Values__c.contains(applicantValue)) && !String.isBlank(compareToValue) && criteria.Field_Values__c.contains(compareToValue) ) {
			comparation = 1;
		}
		return comparation;
	}

}