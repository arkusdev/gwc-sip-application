@isTest
global class GoogleMapsGeocodingAPIMockGenerator {
	
	global class GetGeocodeByPostalCode implements HttpCalloutMock {
	    global HTTPResponse respond(HTTPRequest req) { 
	        HttpResponse res = new HttpResponse();
	        res.setBody(
	        	'<GeocodeResponse>'
				    + '<result>'
				        + '<geometry>'
				            + '<location>'
				                + '<lat>40.7388319</lat>'
				                + '<lng>-73.9815337</lng>'
				            + '</location>'
				        + '</geometry>'
				    + '</result>'
				+ '</GeocodeResponse>'
	        );
	        return res;
	    }
	}
}