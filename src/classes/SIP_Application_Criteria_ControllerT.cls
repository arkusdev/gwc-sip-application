@isTest
public with sharing class SIP_Application_Criteria_ControllerT {

	static testMethod void test1() {

		SIP_Application_Criteria_Controller controller = new SIP_Application_Criteria_Controller();

		SIP_Application_Criteria_Controller.saveCustomSettings('[{"name":"Other","active":true,"fieldName":"race_ethnicity__c","objectName":"Contact","type":"Location"},{"name":"First","active":true,"fieldName":"birthdate","objectName":"Contact","type":"Percentage","operator":"At Least","percentage":"50","fieldValues":[{"name":"2000-10-10"},{"name":"1990-11-20"}]},{"name":"Second","active":true,"fieldName":"stipend_status__c","objectName":"SIP_Application__c","type":"Percentage","operator":"Less Than","percentage":"25","fieldValues":[{"name":"Eligible"}]},{"name":"Third","active":true,"fieldName":"current_grade__c","objectName":"SIP_Application__c","type":"Equals","operator":"","percentage":"","fieldValues":[{"name":"9th"}]},{"name":"Some other rule","active":false,"fieldName":"photourl","objectName":"Contact","type":"Percentage","operator":"At Least","percentage":"10","fieldValues":[{"name":"value1"},{"name":"value2"}]}]');
	
		System.assertEquals(5,[SELECT ID FROM SIP_Application_Criteria__c].size());
	}


}