public with sharing class ApplicationGWCParticipant {
     //Defining and passing variables to be available thru the Process Builder flow
      public class createGWCParticipation {
        @InvocableVariable(label='applicantfirstname')
        public string applicantfirstname;
        @InvocableVariable(label='applicantlastname')
        public string applicantlastname;
        @InvocableVariable(label='applicantemail')
        public string applicantemail;
        @InvocableVariable(label='sipaddresschoices')
        public string sipaddresschoices;
        @InvocableVariable(label='sipaddresschoices2')
        public string sipaddresschoices2;
        @InvocableVariable(label='citychoice1')
        public string citychoice1;
        @InvocableVariable(label='citychoice2')
        public string citychoice2;
        @InvocableVariable(label='nyc1')
        public string nyc1;
        @InvocableVariable(label='bay1')
        public string bay1;
         @InvocableVariable(label='nyc2')
        public string nyc2;
        @InvocableVariable(label='bay2')
        public string bay2;
        @InvocableVariable(label='stage')
        public string stage;
      }
    
     //Method to be invoked from the Process Builder flow
     @InvocableMethod(
       label='Create Applicant'
       description='Create a GWC Participant')
    
     public static void createGWCParicipation(List<createGWCParticipation> requests) {                  
         for (createGWCParticipation req : requests) {
             system.debug('request='+req);
             if(req.citychoice1 == 'New York, NY'){
                 req.sipaddresschoices = req.nyc1;
             }
             if(req.citychoice2 == 'New York, NY'){
                 req.sipaddresschoices2 = req.nyc2;
             }
             if(req.citychoice1 == 'San Francisco Bay Area, CA'){
                 req.sipaddresschoices = req.bay1;
             }
             if(req.citychoice2 == 'San Francisco Bay Area, CA'){
                 req.sipaddresschoices2 = req.bay2;
             }
             if (req.applicantfirstname.length()>0 && req.applicantlastname.length()>0 && req.applicantemail.length() >0 ){
                 ApplicationGWCParticipantHandler.createGWCParticipation(req.applicantfirstname, req.applicantlastname, req.applicantemail,
                                                                            req.sipaddresschoices, req.sipaddresschoices2);
             }
         }
     }
}