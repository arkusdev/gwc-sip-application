global class SIPApplicationSortByProgramBatch implements Database.Batchable<sObject> {
	
	String 	query;
	String 	year;
	Id 		programId;
	Integer programApplicantCount;
	SIP_Placeholder__c placeHolder;
	
	global SIPApplicationSortByProgramBatch(String year, Id programId) {
		this.year 					= year;
		this.programId 			   	= programId;
		this.programApplicantCount 	= 0;
		this.placeHolder = SIP_Placeholder__c.getOrgDefaults();
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		this.query = 'select Name,'
							+ 'ApplicantAssignedCount__c,'
							+ 'Geolocation__latitude__s,'
							+ 'Geolocation__longitude__s,'
							+ 'SIP_Application_Sorted_List__c,'
							+ 'PriorityPercentageMap__c,'
							+ 'Metro_Area__c,'
							+ 'Metro_Area__r.Name,'
							+ 'Metro_Area__r.Region__r.Name,'
							+ 'Metro_Area__r.Region__r.WaitlistedSpots__c'
						+ ' from SIP_Program__c';
		if (String.isBlank(this.placeHolder.ProgramId__c)) {
			this.query += ' where Year_Active__c = \'' + this.year + '\''
						+ ' and Inactive__c = false'
						+ ' and Metro_Area__c != null';
		} else {
			this.query += ' where Id = \'' + this.placeHolder.ProgramId__c + '\'';
		}
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		this.programApplicantCount = 0;
   		
   		SIP.CLASS_CAPACITY 			= 20;
		SIP.WAITLISTED 				= 15;
		SIP.CURRENT_PROGRAM = ((List<SIP_Program__c>) scope)[0];
		SIP.CRITERIA_LIST = [select At_Least_Less_Than__c,
								Field_Name__c,
								Field_Values__c,
								Object_Name__c,
								Particular_Market__c,
								Percent__c,
								Type__c,
								Priority__c,
								Target__c
							from SIP_Application_Criteria__c
							where Active__c = true
							order by Priority__c ASC];				
		Set<String> applicationFields = new Set<String>();
		Set<String> contactFields = new Set<String>();
		
		//Getting the fields needed for the query based on the different Field_Name__c in the SIP_Application_Criteria__c records
		//Criteria rules are always based on 2 objects "SIP_Application_Criteria__c" & "Contact"
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if (criteria.Object_Name__c == 'SIP_Application__c')
				applicationFields.add(criteria.Field_Name__c);
			if (criteria.Object_Name__c == 'Contact')
				contactFields.add(criteria.Field_Name__c);
		}
		
		String applicationQuery = 'select Applicant__c,'
										+ 'Acceptance_Status__c,'
										+ 'Acceptance_Status_Determination__c,'
										+ 'Category__c,'
										+ 'Updated_by_Placement_Algorithm__c'; 
		if (!applicationFields.isEmpty())										
			applicationQuery += ',' + String.join(new List<String>(applicationFields), ',') ;
		applicationQuery += ' from SIP_Application__c'
								+ ' where Application_Year__c = \'' + this.year  + '\''
								+ ' and SIP_Stage__c = \'Holding Bucket\'';
		SIP.APPLICATION_MAP = new Map<Id, SIP_Application__c>((List<SIP_Application__c>)Database.query(applicationQuery));
		
		//getting all the Contacts from the application records
		List<Id> contactIds = new List<Id>();
		for (SIP_Application__c application : SIP.APPLICATION_MAP.values())
			contactIds.add(application.Applicant__c);
		
		String contactQuery = 'select MailingLatitude,'
									+ 'MailingLongitude,'
									+ '('
										+ 'select Participant__c,'
										+ 'SIP_Program__r.Name' 
										+ ' from SIP_Students__r' 
										+ ' where SIP_Program__c = \'' + SIP.CURRENT_PROGRAM.Id + '\''
									+ ')';
		if (!contactFields.isEmpty())										
			contactQuery += ',' + String.join(new List<String>(contactFields), ',') ;
		contactQuery += ' from Contact' 
						+ ' where Id IN (\'' + String.join(contactIds, '\',\'') + '\')';
		SIP.CONTACT_MAP = new Map<Id, Contact>((List<Contact>)Database.query(contactQuery));
		
		Region__c region;
		if (String.isBlank(this.programId)) {
			region = [select WaitlistedSpots__c, Name from Region__c where Id =: SIP.CURRENT_PROGRAM.Metro_Area__r.Region__c];
			region.WaitlistedSpots__c += 15;
		}
		
		SIP.PROGRAM_PERCENTAGE = new SIP.ProgramPercentage();
		List<SIP_Applicant> applicantList = new List<SIP_Applicant>();
		
		for (SIP_Application__c application : SIP.APPLICATION_MAP.values()) {
			if (SIP.CONTACT_MAP.containsKey(application.Applicant__c) && SIP.CONTACT_MAP.get(application.Applicant__c).SIP_Students__r != null && SIP.CONTACT_MAP.get(application.Applicant__c).SIP_Students__r.size() > 0) {
				
				if (String.isBlank(application.Acceptance_Status_Determination__c) && String.isBlank(application.Category__c)) {
					applicantList.add(new SIP_Applicant(application));
				} else if (application.Category__c == SIP.CURRENT_PROGRAM.Name) {
					this.programApplicantCount++;
					programCriteriaPercentageCalc(application);
				} else if (String.isBlank(this.placeHolder.ProgramId__c) && !String.isBlank(application.Category__c) && application.Category__c.contains('Wait-listed:')) {
					String regionName = application.Category__c.split(': ')[1];
					if (regionName == region.Name)
						region.WaitlistedSpots__c--;
				}
			}

		}

		applicantList.sort();
		List<Id> idsList = new List<Id>();
		for(SIP_Applicant applicant : applicantList) 
			idsList.add(applicant.applicant.Id);
		SIP.CURRENT_PROGRAM.SIP_Application_Sorted_List__c = String.join(idsList, ';');
		List<String> priorityPercentageList = new List<String>();
		for (Integer key : SIP.PROGRAM_PERCENTAGE.criteriaPriorityToNumber.keySet())
			priorityPercentageList.add(key + ':' + SIP.PROGRAM_PERCENTAGE.criteriaPriorityToNumber.get(key));
		SIP.CURRENT_PROGRAM.PriorityPercentageMap__c = String.join(priorityPercentageList, ';');
		SIP.CURRENT_PROGRAM.ApplicantAssignedCount__c = this.programApplicantCount;
		update SIP.CURRENT_PROGRAM;
		if (String.isBlank(this.placeHolder.ProgramId__c)) 
			update region;
	}
	
	global void finish(Database.BatchableContext BC) {
		if (!Test.isRunningTest()) {
			Database.executeBatch(new SIPApplicationAssignmentBatch(this.year, this.placeHolder.ProgramId__c));
		}
	}

	//Calculates based on the new application assigned to the program where the percentage of each "Percentage" "At Least" criteria rule are on the program.
	private static void programCriteriaPercentageCalc(SIP_Application__c application) {
		String[] applicantSplittedValues;
		for (SIP_Application_Criteria__c criteria : SIP.CRITERIA_LIST) {
			if ((String.isEmpty(criteria.Particular_Market__c) || criteria.Particular_Market__c == SIP.CURRENT_PROGRAM.Metro_Area__r.Name) && criteria.Type__c == 'Percentage' && criteria.At_Least_Less_Than__c == 'At Least' && criteria.Target__c != 'National') {
				if (criteria.Object_Name__c == 'SIP_Application__c') {
					applicantSplittedValues = String.isBlank(String.valueOf(application.get(criteria.Field_Name__c))) ? null : String.valueOf(application.get(criteria.Field_Name__c)).split(';');
				} else if (criteria.Object_Name__c == 'Contact') {
					applicantSplittedValues = String.isBlank(String.valueOf(SIP.CONTACT_MAP.get(application.Applicant__c).get(criteria.Field_Name__c))) ? null : String.valueOf(SIP.CONTACT_MAP.get(application.Applicant__c).get(criteria.Field_Name__c)).split(';');
				}
				if (applicantSplittedValues!= null) {
					for (String value : applicantSplittedValues) {
						if (!String.isBlank(value) && criteria.Field_Values__c.contains(value)) {
							SIP.PROGRAM_PERCENTAGE.calcPercentageByAddingOne(Integer.valueOf(criteria.Priority__c), Integer.valueOf(SIP.CURRENT_PROGRAM.ApplicantAssignedCount__c), Integer.valueOf(criteria.Percent__c));
							break;
						}
					}
				}
			}
		}
	}
	
}