@isTest
private class Test_Batch_Update {

    static testmethod void test() {
        // The query used by the batch job.
        String studentfname = 'Lancy';
        String studentlname = 'Tan';
        String query = 'SELECT Id  FROM SIP_Application__c'+
            'WHERE student_first_name__c = :studentfname AND student_last_name__c = :studentlname'  ;
                   

      

       Test.startTest();
       FindCBOs c = new FindCBOs(query);
       Database.executeBatch(c);
       Test.stopTest();

      
    }
}