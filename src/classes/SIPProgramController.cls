public class SIPProgramController {
@AuraEnabled
    public static List<SIP_Program__c> getPrograms() {
        List<SIP_Program__c> sipsupdated = new List<SIP_Program__c>();
        List<SIP_Program__c> sips = [Select Id, Name,Applicant_Count__c  From SIP_Program__c where Name like '2016%' order by Name];
    /*    for(SIP_Program__c sip: sips){
            sip.Applicant_Count__c = [Select count() from SIP_Student__c where SIP_Program__r.name = :sip.name];
            sipsupdated.add(sip);
            System.debug('Count: '+sip.Applicant_Count__c); 
        } */
        return sips;
    }
    @AuraEnabled
    public  Static SIP_Program__c getProgramByName(String name) {
        return [Select Id, Name,Applicant_Count__c From SIP_Program__c where name = :name];
    }
 /*   @AuraEnabled
    public static List<SIP_Student__c> getStudentsByProgram(String program) {
        return [Select Id, Participant__r.Name, SIP_Stage__c From SIP_Student__c 
                where SIP_Program__r.name = :program and Participant__r.Name != ''
                order by SIP_Stage__c, Participant__r.FirstName];
    } */
    @AuraEnabled
    public static List<Contact> getFullStudentsByProgram(String program, String stage) {
        return [Select Id, Name,Email,SIP_Stage__c, Community_Organization_Referrer__c, Community_Organization_Referrer_Other__c,Race_Ethnicity__c, If_Multiracial__c,If_Applicant_Selected_Asian__c,
                Qualify_for_Free_Reduced_Lunch__c,Member_of_a_GWC_Club__c,test__c ,How_did_you_find_out_Other__c, MailingCity, MailingState,
                (Select Program_City__c,Program_City_2nd_Choice__c,Cumulative_Score__c,School_District__c, School_Name__c, Category__c from SIP_Applications__r  order by createddate desc limit 1 ),
                (Select SIP_Program__r.Name From  SIP_Students__r WHERE SIP_Program__r.name != :program) FROM Contact
                where  SIP_Stage__c = :stage and  id in (Select participant__c from SIP_Student__c 
                                                        WHERE SIP_Program__r.name = :program) order by SIP_Stage__c DESC];
    }
    
/*@AuraEnabled
    public static SIP_Student__c getStudent( Id sid, String program) {
        System.debug('In Controller getStudent: ' + sid);
        return [Select Participant__r.Id, Participant__r.Name, SIP_Stage__c, Particpant_Email__c From SIP_Student__c 
                where SIP_Program__r.name = :program and Participant__r.Id = :sid
                order by SIP_Stage__c, Participant__r.FirstName];
    }
    @AuraEnabled
    public static SIP_Student__c getParticipantByIdandProgram(String program, Id sid) {
        return [Select Participant__r.Id, Participant__r.Name, SIP_Stage__c, Particpant_Email__c, Participant_Type__c, SIP_Program__r.name From SIP_Student__c 
                where SIP_Program__r.name = :program and Participant__r.Id = :sid];
        
        
    } */
}