public class ApplicationGWCParticipantHandler {
//Builds a SIP Applicant Participant relationship from the Fluid Review Application form integration  
   
    @future  //@Future ensures the relationship occurs after the Student Contact record has 
    //committed to the database.
    
    public static void createGWCParticipation(string sfname, string slname, string semail, string sipchoices, string sipchoices2){        
        Integer cnt=0;
        //select all the 2016 SIP Programs 
        SIP_Program__c[] SipPrograms = [SELECT   Application_Form_Label__c, Id, Applicant_Count__c, name FROM SIP_Program__c WHERE Year_Active__c = '2017' AND Application_Form_Label__c != ''];
        List<SIP_Student__c> participants = new List<SIP_Student__c>();
        List<SIP_Student__c> participantstoDelete = new List<SIP_Student__c>();
        List<SIP_Program__c> sipstoUpdate = new List<SIP_Program__c>();
        Set<SIP_Program__c> sipstoUpdateSet = new Set<SIP_Program__c>();
        List<SIP_Program__c> sipstoDecrement = new List<SIP_Program__c>();
        Set<SIP_Program__c> sipstoDecrementSet = new Set<SIP_Program__c>();

        
        // All the SIP locations end with a ) and Fluid review separates them with a comma.  We change 
        // the comma to a ; for the split.
        String inchoices = sipchoices.replace('),', ');');
        String inchoices2 = sipchoices2.replace('),', ');');
        System.debug('Inchoices: '+inchoices );
        System.debug('Inchoices2: '+inchoices2 );
        inchoices = inchoices + ';'+ inchoices2;
        String[] choices = inchoices.split('[;]');
        Set<String> choiceset = new Set<String>();
        choiceset.addall(choices);
        
      
        List<Contact> StudentID = [SELECT ID,firstname, lastname,email,FLTaskProgress__c,Coding_Experience__c, Computer_Interests__c,Region_of_Interest__c,
                         Daily_access_to_tech_at_home__c, Employer_Name__c,Interest_Type__c,TeacherInterest_Current_Role__c,Student_Grade_Level__c,
                         Race_ethnicity_additional_detail__c, Family_who_works_in_tech__c, H_S_Graduation_Year__c, School__c,
                         How_are_you_connected_to_the_host_site__c,How_did_you_find_out_Other__c,How_did_you_find_out_about_GWC_Clubs__c,
                         Interest_Form_Contact_Type__c,If_Applicant_Selected_Asian__c,If_Multiracial__c, InterestFormSubmitDate__c ,
                         Club_Vol_Occupation__c,Job_Title__c,Partnership_Region_of_Interest__c, Partner_Working_Relationship__c,AccountId,
                         Partner_Working_Relationship_Other__c, Race_Ethnicity__c, Teaching_Experience__c,Current_Position__c,Teaching_Position__c,Type_of_Club_Location__c,
                         Type_of_Donor_Support__c,US_Presence__c,Disable_Triggers__c,What_does_your_company_do__c,What_is_your_target_audience__c, Where_will_the_club_be_hosted__c
                                   
                                   FROM Contact WHERE (email = :semail AND FirstName = :sfname and LastName = :slname
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        
        //Do we have a contact record to use      
        if(StudentID.size() > 0){
            //Delete any existing Applicant to SIP Relationships
            participantstoDelete = [SELECT Id, SIP_Program__c from SIP_Student__c WHERE Participant__c = :StudentID[0].id 
                                   AND Participant_Type__c = 'SIP Applicant' AND isdeleted = false];
           if(participantstoDelete.size()>0){
             for(SIP_Student__c participant: participantstoDelete){
                  
                    SIP_Program__c[] decrementPrograms = [SELECT ID, name, Applicant_Count__c from SIP_Program__c 
                                                       WHERE ID = :participant.SIP_Program__c];
                    if(decrementPrograms.size() == 1 ){
                      decrementPrograms[0].Applicant_count__c--;
                      sipstoDecrementSet.add(decrementPrograms[0]);
                      sipstoDecrement.add(decrementPrograms[0]);
                    }     
                   System.Debug('indecerment pgm:'+ decrementPrograms[0].name);
              }
             
              delete participantstoDelete;
              Database.emptyRecycleBin(participantstoDelete);
            }
            //Check for an existing participation so as to not duplicate
           for(String choice: choiceset){
               System.debug('In For Choice: '+ choice);
               
               String SIPId;
               for(SIP_Program__c pgm: SipPrograms){
                 //  System.debug('Label from Array: '+pgm.Application_Form_Label__c);
                   if(pgm.Application_Form_Label__c.trim() == choice.trim()){
                       SIPId = pgm.Id;
                       System.debug('LabelFound: for choice: ' +choice +' SIPId: ' +SIPId );
                
                       SIP_Student__c participant = new SIP_Student__c();
                       participant.Participant__c = StudentId[0].Id;
                       participant.SIP_Program__c = SIPId;
                       participant.Participant_Type__c = 'SIP Applicant';
                    //   System.debug('About to insert StudentContactid: '+StudentId[0]+' SIPId: '+SIPId);
                       participants.add(participant); 
                   //  System.Debug('increment pgm:'+ pgm.name);
                  
               if(!findSIPinDecrements(sipstoDecrement, pgm)){
                           pgm.Applicant_Count__c++; 
               }
                       sipstoUpdateSet.add(pgm);              
                 }   else {
                // System.Debug('LabelNotFound for: ' + choice);
                 }              
             }
         }
            
     ContactCheckLead(studentID);  
     }
     else {
        System.debug('In ApplicationGWCParticipantHandler. Contact Record not found for: ' +slname );
    }
    if(participants.size()>0){
       insert participants;
    }
    if(sipstoDecrementSet.size() > 0){
        sipstoDecrement.clear();
        sipstoDecrement.addAll(sipstoDecrementSet);
        update sipstoDecrement;
    }
    if(sipstoUpdateSet.size() > 0){
        sipstoUpdate.addAll(sipstoUpdateSet);
        update sipstoUpdate;
    }
  }

   private static boolean findSIPinDecrements(SIP_Program__c[] decs, SIP_Program__c target){
    for(SIP_Program__c sip: decs){
        if(sip.Id == target.Id){
     //       System.Debug('Got it is Subroutine '+ target.name);
            return true;
        }
    }
  //  System.Debug('Subroutine did not find: '+ target.name);
    return false;
   }
   private static void ContactCheckLead (Contact[] cons) {
 System.debug('Check Contact Lead ');
    List<Lead> leadtobedeleted = new List <Lead>();
    List<Contact> contactsToUpdate = new List <Contact>();
    if(cons.size() > 0){
        for (Contact con: cons){
      if(!con.Disable_Triggers__c){
        //Check for value that comes only from FluidReview SIP Application
        if(con.FLTaskProgress__c > 0) { 
            Lead[] leadmatch;
            System.debug('In Trigger '+ con.FirstName +' FLTask: ' + con.FLTaskProgress__c +'  ' + con.LastName + ' email '+con.Email);
            leadmatch = [SELECT ID,firstname, lastname,email,Coding_Experience__c, Computer_Interests__c,Region_of_Interest__c,
                         Daily_access_to_tech_at_home__c, Employer_Name__c,Interest_Type__c,TeacherInterest_Current_Role__c,Student_Grade_Level__c,
                         Ethnicity_additional_information__c, Family_who_works_in_tech__c, Expected_H_S_Graduation_Year__c, School__c,
                         How_are_you_connected_to_the_host_site__c,How_did_you_find_out_Other__c,How_did_you_hear_about_GWC__c,
                         Interest_Form_Contact_Type__c,If_Applicant_Selected_Asian__c,If_Multiracial__c, InterestFormSubmitDate__c ,
                         Club_Vol_Occupation__c,Job_Title__c,Partner_Region_of_Interest__c, Partner_Type__c,Partner_Working_Relationship__c,
                         Partner_Working_Relationship_Other__c, Race_Ethnicity__c, Teaching_Experience__c,Current_Position__c,Teaching_Position__c,Type_of_Club_Location__c,
                         Type_of_Donor_Support__c,US_Presence__c,What_does_your_company_do__c,What_is_your_target_audience__c, Where_will_the_club_be_hosted__c FROM Lead 
                        WHERE (email = :con.email AND FirstName = :con.firstname and LastName = :con.LastName AND IsConverted = false
                                            ) ORDER BY createddate ASC NULLS FIRST LIMIT 1];
        
            if(leadmatch.size() > 0){
                for(Lead ld: leadmatch){
                    
                    System.debug('Org: '+con.AccountId );
                    System.debug('Match Found');
                    if(con.Coding_Experience__c == '')
                        con.Coding_Experience__c = ld.Coding_Experience__c;
                    if(con.Computer_Interests__c == '')
                        con.Computer_Interests__c = ld.Computer_Interests__c;
                    if(con.Computer_Interests__c == '')
                        con.Daily_access_to_tech_at_home__c = ld.Daily_access_to_tech_at_home__c;
                    if(con.Race_Ethnicity__c == '')
                        con.Race_Ethnicity__c = ld.Race_Ethnicity__c;
                    if(con.If_Multiracial__C == '')
                        con.If_Multiracial__C = ld.If_Multiracial__c;
                    if(con.Race_ethnicity_additional_detail__c == '')
                         con.Race_ethnicity_additional_detail__c = ld.Ethnicity_additional_information__c;
                    if(con.H_S_Graduation_Year__c == '')
                        con.H_S_Graduation_Year__c = ld.Expected_H_S_Graduation_Year__c;
                    if(con.How_did_you_find_out_about_GWC_Clubs__c == '')
                        con.How_did_you_find_out_about_GWC_Clubs__c =  ld.How_did_you_hear_about_GWC__c;
                   // if(con.How_did_you_find_out_Original__c == '')
                    con.How_did_you_find_out_Original__c =  ld.How_did_you_hear_about_GWC__c;
                    if(con.Student_Grade_Level__c == '')
                        con.Student_Grade_Level__c = ld.Student_Grade_Level__c;
                    
                    con.Employer_Name__c = ld.Employer_Name__c;
                   
                    con.Interest_Type__c = ld.Interest_Type__c;
                    
                    con.TeacherInterest_Current_Role__c = ld.TeacherInterest_Current_Role__c;
                    
                    con.Race_ethnicity_additional_detail__c = ld.Ethnicity_additional_information__c;
                    
                    con.Family_who_works_in_tech__c = ld.Family_who_works_in_tech__c;
                   
                   
                    con.School__c = ld.School__c;
                    
                    con.How_are_you_connected_to_the_host_site__c = ld.How_are_you_connected_to_the_host_site__c;
                    
                    con.How_did_you_find_out_Other__c = ld.How_did_you_find_out_Other__c;
                     
                    con.Interest_Form_Contact_Type__c = ld.Interest_Form_Contact_Type__c;
                   
                    con.If_Applicant_Selected_Asian__c = ld.If_Applicant_Selected_Asian__c;
                    
                    
                    
                    con.InterestFormSubmitDate__c = ld.InterestFormSubmitDate__c;
                    
                    con.Job_Title__c = ld.Job_Title__c;
                   
                    con.Partnership_Region_of_Interest__c = ld.Partner_Region_of_Interest__c;
                   
                    con.Club_Vol_Occupation__c = ld.Club_Vol_Occupation__c;
                    //con.Partner_Working_Relationship_Other__c =  ld.Partnership_Additional_Information__c;
                    con.Partner_Working_Relationship__c = ld.Partner_Working_Relationship__c;
                    
                    con.Partner_Working_Relationship_Other__c = ld.Partner_Working_Relationship_Other__c;
                    
                    
                    con.Region_Of_Interest__c = ld.Region_of_Interest__c;
                    
                    con.Teaching_Experience__c = ld.Teaching_Experience__c;
                    con.Current_Position__c = ld.Current_Position__c;
                    con.Teaching_Position__c = ld.Teaching_Position__c;
                    con.Type_of_Club_Location__c = ld.Type_of_Club_Location__c;
                    con.Type_of_Donor_Support__c = ld.Type_of_Donor_Support__c;
                    con.US_Presence__c = ld.US_Presence__c;
                    //ld.website;
                    con.What_does_your_company_do__c = ld.What_does_your_company_do__c;
                    con.What_is_your_target_audience__c = ld.What_is_your_target_audience__c;
                    con.Club_Host_Location__c = ld.Where_will_the_club_be_hosted__c;
                    

                    Database.LeadConvert lc = new Database.LeadConvert();
                    lc.setLeadId(ld.id);

                    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    lc.setContactId(con.id);
                    lc.setAccountId(con.accountId);
                    System.Debug('The Contact ID '+ con.id);
                    lc.setDoNotCreateOpportunity(TRUE);
                    Database.LeadConvertResult lcr = Database.convertLead(lc);
                //  System.assert(lcr.isSuccess());
                    
                    //leadtobedeleted.add(ld);
                    
                    contactsToUpdate.add(con);
                }
            }
                
            else
                System.debug('No Match Found');
        }
          
        
        
          if(contactsToUpdate.size() > 0){
            update contactsToUpdate;
        }
      }
      con.Disable_Triggers__c = false;
     }
    }  
   }
}