public with sharing class ClubUtilities {

    public static String getContactID(string emailAddress) {
        //NOTE: in SELECT below the record type is hardcoded to the student record type id in production
       //PROD RECORDTYPE 
        Contact[] contact =  [SELECT ID,email FROM Contact WHERE email = :emailAddress AND recordtype.name = 'Student' order by createddate limit 1];
        //IHDev Student Record type below
      //  Contact[] contact =  [SELECT ID,email FROM Contact WHERE email = :emailAddress AND recordtypeid = '012J00000005Sjn' order by createddate limit 1];       
               
               if(contact.size() > 0 ){
                  return contact[0].ID;        }
            return '';
    }
    
    public static String getGeneralContactID(string emailAddress) {
        //NOTE: in SELECT below the record type is hardcoded to the student record type id in production
       //PROD RECORDTYPE 
        Contact[] contact =  [SELECT ID,email FROM Contact WHERE email = :emailAddress AND recordtype.name = 'General Contact' order by createddate limit 1];
            if(contact.size() > 0 ){
                  return contact[0].ID;        }
            return '';
    }    
   /* public static void linkEnrollmentFormParticipants(){
              Club_Student_Enrollment_POC__c[] enforms = [Select Student_Email__c, Student_First_Name__c, Student_Last_Name__c from Club_Student_Enrollment_POC__c];
      //  List<Contact> cons = [Select id, firstname, lastname,email from Contact where CreatedByID = '005G0000006qv9H']; // sandbox id
        List<Contact> cons = [Select id, firstname, lastname,email from Contact where CreatedByID = '005G0000007bIqi']; //prod id
        Map<String, Club_Student_Enrollment_POC__c> sforms = new Map<String, Club_Student_Enrollment_POC__c>();
        //sforms.addAll(enforms);
        for (Club_Student_Enrollment_POC__c e: enforms){
            String StudentFormCat = e.Student_First_Name__c + e.Student_Last_Name__c + e.Student_Email__c;
            SYSTEM.debug('CatFrom Forms'+StudentFormCat);
            sforms.put(StudentFormCat, e);
        }
        List<Club_Student_Enrollment_POC__c> updateCons = new List<Club_Student_Enrollment_POC__c>();
        for(Contact c: cons){
            String DBCat = c.FirstName + c.LastName + c.Email;
            SYSTEM.debug('CatFrom Contacts'+DBCat);
            if(sforms.containsKey(DBCat)){
                Club_Student_Enrollment_POC__c UPDATEForm = sforms.get(DBCat);
                UPDATEForm.Participant__c = c.ID;
                SYSTEM.debug('In Contact Loop added Participant '+DBCat);
                updateCons.add(UPDATEForm);
            }   
           
        }
        if(updateCons.size() > 0){
                update updateCons;
        }
    } */

}