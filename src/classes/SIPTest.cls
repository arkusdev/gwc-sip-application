@isTest
private class SIPTest {
	
	private static void createCriteriaRules() {
		List<SIP_Application_Criteria__c> criteriaList = new List<SIP_Application_Criteria__c>();
		SIP_Application_Criteria__c criteria1 = new SIP_Application_Criteria__c();
		criteria1.Name = 'First';
		criteria1.Type__c = 'Percentage';
		criteria1.Percent__c = 50;
		criteria1.Object_Name__c = 'Contact';
		criteria1.Field_Name__c = 'Race_Ethnicity__c';
		criteria1.Field_Values__c = 'Hispanic or Latino;Black or African American';
		criteria1.At_Least_Less_Than__c = 'At Least';
		criteria1.Active__c = true;
		criteria1.Priority__c = 1;

		SIP_Application_Criteria__c criteria2 = new SIP_Application_Criteria__c();
		criteria2.Name = 'Second';
		criteria2.Type__c = 'Percentage';
		criteria2.Percent__c = 25;
		criteria2.Object_Name__c = 'SIP_Application__c';
		criteria2.Field_Name__c = 'Stipend_Status__c';
		criteria2.Field_Values__c = 'Eligible';
		criteria2.At_Least_Less_Than__c = 'Less Than';
		criteria2.Active__c = true;
		criteria2.Priority__c = 2;

		SIP_Application_Criteria__c criteria3 = new SIP_Application_Criteria__c();
		criteria3.Name = 'Third';
		criteria3.Type__c = 'Equals';
		criteria3.Object_Name__c = 'SIP_Application__c';
		criteria3.Field_Name__c = 'Current_Grade__c';
		criteria3.Field_Values__c = '9th';
		criteria3.Active__c = true;
		criteria3.Priority__c = 3;

		SIP_Application_Criteria__c criteria4 = new SIP_Application_Criteria__c();
		criteria4.Name = 'Fourth';
		criteria4.Type__c = 'Equals';
		criteria4.Object_Name__c = 'Contact';
		criteria4.Field_Name__c = 'Age_Group__c';
		criteria4.Field_Values__c = '18 - 24';
		criteria4.Active__c = true;
		criteria4.Priority__c = 4;

		SIP_Application_Criteria__c criteria5 = new SIP_Application_Criteria__c();
		criteria5.Name = 'Fifth';
		criteria5.Type__c = 'Location';
		criteria5.Active__c = true;
		criteria5.Priority__c = 5;
		criteriaList.add(criteria1);
		criteriaList.add(criteria2);
		criteriaList.add(criteria3);
		criteriaList.add(criteria4);
		criteriaList.add(criteria5);
		insert criteriaList;
	}

	static testMethod void ClassAssignmentTest() {
		String YEAR = '2017';
		createCriteriaRules();
		List<Region__c> regionList = new List<Region__c>();
		Region__c region1 = new Region__c(Name = '1');
		Region__c region2 = new Region__c(Name = '2');
		regionList.add(region1);
		regionList.add(region2);
		insert regionList;

		List<Metro_Area__c> marketList = new List<Metro_Area__c>();
		Metro_Area__c market1 = new Metro_Area__c();
		market1.Name = 'Market1';
		market1.Region__c = region1.Id;

		Metro_Area__c market2 = new Metro_Area__c();
		market2.Name = 'Market2';
		market2.Region__c = region2.Id;
		marketList.add(market1);
		marketList.add(market2);
		insert marketList;

		List<SIP_Program__c> programList = new List<SIP_Program__c>();
		SIP_Program__c program1 = new SIP_Program__c();
		program1.Name = 'Program1';
		program1.Metro_Area__c = market1.Id;
		program1.Year_Active__c = YEAR;
		program1.Geolocation__latitude__s = 40.7135097;
        program1.Geolocation__longitude__s = -73.9859414;

		SIP_Program__c program2 = new SIP_Program__c();
		program2.Name = 'Program2';
		program2.Metro_Area__c = market1.Id;
		program2.Year_Active__c = YEAR;
		program2.Geolocation__latitude__s = 40.7135000;
        program2.Geolocation__longitude__s = -73.9859400;

		SIP_Program__c program3 = new SIP_Program__c();
		program3.Name = 'Program3';
		program3.Metro_Area__c = market2.Id;
		program3.Year_Active__c = YEAR;
		program3.Geolocation__latitude__s = 40.7134010;
        program3.Geolocation__longitude__s = -73.9859100;
		programList.add(program1);
		programList.add(program2);
		programList.add(program3);
		insert programList;
		
		List<Contact> contactList = new List<Contact>();
		for (Integer i = 0; i < 10; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Hispanic or Latino';
			contact.MailingLatitude = Decimal.valueOf('40.7388' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9811' + i + '33' + i);
        	contact.Age_Group__c = '25 - 34';
			contactList.add(contact);
		}
		for (Integer i = 10; i < 40; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Asian';
			contact.MailingLatitude = Decimal.valueOf('40.7300' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9701' + i + '33' + i);
        	contact.Age_Group__c = '18 - 24';
			contactList.add(contact);
		}
		for (Integer i = 40; i < 70; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Black or African American';
			contact.MailingLatitude = Decimal.valueOf('40.7390' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9100' + i + '33' + i);
        	contact.Age_Group__c = '18 - 24';
			contactList.add(contact);
		}
		insert contactList;

		List<SIP_Application__c> applicationList = new List<SIP_Application__c>();
		for (Integer i = 0; i < 15; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Eligible';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '9th';
			applicationList.add(application);
		}

		for (Integer i = 15; i < 40; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Awarded';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '8th';
			applicationList.add(application);
		}

		for (Integer i = 40; i < 70; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Ineligible';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '9th';
			applicationList.add(application);
		}
		insert applicationList;

		List<SIP_Student__c> studentList = new List<SIP_Student__c>();
		for(Integer i = 0; i < 5; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program2.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
		}
		for(Integer i = 5; i < 15; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
		}
		for(Integer i = 15; i < 20; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program2.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
		}
		for(Integer i = 20; i < 30; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program2.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
		}
		for(Integer i = 30; i < 45; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
		}
		for(Integer i = 45; i < 50; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program2.Id;
			studentList.add(student);
		}
		for(Integer i = 50; i < 70; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program2.Id;
			studentList.add(student);
			student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program3.Id;
			studentList.add(student);
		}
		insert studentList;
		Test.startTest();
			SIP.ClassAssignment('2017');
		Test.stopTest();
		
	}

	static testMethod void ClassAssignmentTest2() {
		String YEAR = '2017';
		createCriteriaRules();
		List<Region__c> regionList = new List<Region__c>();
		Region__c region1 = new Region__c(Name = '1');
		Region__c region2 = new Region__c(Name = '2');
		regionList.add(region1);
		regionList.add(region2);
		insert regionList;

		List<Metro_Area__c> marketList = new List<Metro_Area__c>();
		Metro_Area__c market1 = new Metro_Area__c();
		market1.Name = 'Market1';
		market1.Region__c = region1.Id;
		marketList.add(market1);
		insert marketList;

		List<SIP_Program__c> programList = new List<SIP_Program__c>();
		SIP_Program__c program1 = new SIP_Program__c();
		program1.Name = 'Program1';
		program1.Metro_Area__c = market1.Id;
		program1.Year_Active__c = YEAR;
		program1.Geolocation__latitude__s = 40.7135097;
        program1.Geolocation__longitude__s = -73.9859414;

		programList.add(program1);
		insert programList;
		
		List<Contact> contactList = new List<Contact>();
		for (Integer i = 0; i < 10; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Hispanic or Latino';
			contact.MailingLatitude = Decimal.valueOf('40.7388' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9811' + i + '33' + i);
        	contact.Age_Group__c = '25 - 34';
			contactList.add(contact);
		}
		for (Integer i = 10; i < 40; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Asian';
			contact.MailingLatitude = Decimal.valueOf('40.7300' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9701' + i + '33' + i);
        	contact.Age_Group__c = '18 - 24';
			contactList.add(contact);
		}
		for (Integer i = 40; i < 70; i++) {
			Contact contact = new Contact();
			contact.LastName = 'Contact' + i;
			contact.Race_Ethnicity__c = 'Black or African American';
			contact.MailingLatitude = Decimal.valueOf('40.7390' + i + '31' + i);
        	contact.MailingLongitude = Decimal.valueOf('-73.9100' + i + '33' + i);
        	contact.Age_Group__c = '18 - 24';
			contactList.add(contact);
		}
		insert contactList;

		List<SIP_Application__c> applicationList = new List<SIP_Application__c>();
		for (Integer i = 0; i < 15; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Eligible';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '9th';
			application.Category__c = program1.Name;
			application.Acceptance_Status_Determination__c = 'Final';
			applicationList.add(application);
		}

		for (Integer i = 15; i < 40; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Awarded';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '8th';
			applicationList.add(application);
		}

		for (Integer i = 40; i < 70; i++) {
			SIP_Application__c application = new SIP_Application__c();
			application.Applicant__c = contactList[i].Id;
			application.SIP_Stage__c = 'Holding Bucket';
			application.Stipend_Status__c = 'Ineligible';
			application.Application_Year__c = YEAR;
			application.Current_Grade__c = '9th';
			applicationList.add(application);
		}
		insert applicationList;

		List<SIP_Student__c> studentList = new List<SIP_Student__c>();
		for(Integer i = 0; i < 70; i++) {
			SIP_Student__c student = new SIP_Student__c();
			student.Participant__c = contactList[i].Id;
			student.SIP_Program__c = program1.Id;
			studentList.add(student);
		}
		insert studentList;
		Test.startTest();
			SIP.ClassAssignment('2017');
			Database.executeBatch(new SIPApplicationSortByProgramBatch('2017', null));
			Database.executeBatch(new SIPApplicationAssignmentBatch('2017', null));
		Test.stopTest();
	}
	
}