global class GetProgramLocationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
	
	List<Id> programIds;
	String query;
	
	global GetProgramLocationBatch(List<Id> programIds) {
		this.programIds = programIds;
	}


	global Database.QueryLocator start(Database.BatchableContext BC) {
		if (this.programIds != null) {
			query = 'select SIP_Classroom_Address_ZIP_CODE__c from SIP_Program__c where Id IN (\'' + String.join(this.programIds, '\',\'') + '\')';
		} else {
			query = 'select SIP_Classroom_Address_ZIP_CODE__c from SIP_Program__c where Geolocation__latitude__s = null and Geolocation__longitude__s = null and SIP_Classroom_Address_ZIP_CODE__c != null and Year_Active__c = \'2017\'';
		}
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<SIP_Program__c> programList = (List<SIP_Program__c>)scope;
		for (SIP_Program__c program : programList) {
			System.Location location 			= Geolocation.GetLocation(program.SIP_Classroom_Address_ZIP_CODE__c);
			if (location != null) {
				program.Geolocation__latitude__s 	= location.getLatitude();
				program.Geolocation__longitude__s 	= location.getLongitude();
			}
		}
		update programList;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}