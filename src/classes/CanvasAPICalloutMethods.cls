public with sharing class CanvasAPICalloutMethods {   
    //Class performs specific functions on Canvas such as creating a Student user or enrolling student via Canvas API call out. 
    //Indirectly used by Process Builder flow via Apex class file CanvasEnrollStudentPOC.
    //Leverages Canvas API class for connecting with Canvas.
    
    @future (callout=true) //When invoked from creating a new Contact, @Future ensures the call out occurs after the Contact has
    //committed to the database.
    public static void createStudentUser_CalloutFromFuture(string fname, string lname, string shortname, string uniquename, string email){
        String SIS = '';
        
        SIS = ClubUtilities.getContactID(email);
        String method = 'POST';
        String body = 'user[name]='+fname+' '+lname+'&user[terms_of_use]=true&user[short_name]='+shortname+'&pseudonym[unique_id]='+uniquename+'&pseudonym[force_self_registration]=true&pseudonym[sis_user_id]='+SIS;
        system.debug('bodystring='+body);
        String endpoint = 'https://girlswhocode.instructure.com/api/v1/accounts/1/users?access_token=5910~fxMFKc6OpPbXkV6XyKjA5Yuab4OYqk94PIagoqeTl580lWXV780rRGGDrh2jVSup';
        httpResponse res = canvasAPI.callout(method, endpoint, body);
        system.debug('res='+res);
        
     }
     
   //public void enrollInClub_Callout(){
     
   //  String method = 'POST';
   //  String body = 'enrollment[user_id]=1610&enrollment[type]=StudentEnrollment&enrollment[enrollment_state]=active';
   //  String endpoint = 'https://girlswhocode.instructure.com/api/v1/courses/87/enrollments?access_token=5910~fxMFKc6OpPbXkV6XyKjA5Yuab4OYqk94PIagoqeTl580lWXV780rRGGDrh2jVSup';
   //  httpResponse res = canvasAPI.callout(method, endpoint, body);
   //}
    
}