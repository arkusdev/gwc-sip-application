@isTest(SeeAllData=true)
public class TestClubTriggerHandler {

    static testmethod void test_ClubTrig () {
        
      	Account o = new Account();
        //Create new account
        
        o.Name = 'Test Organization';
        o.BillingStreet = '1313 Mocking Bird Lane';
        o.BillingCity = 'Mocking Bird Nest';
        o.BillingCountry = 'Mocking Bird Island';
        o.BillingState = 'Mocking Bird Flock';
        o.BillingPostalCode = '11111';
        o.Club_Host_Location_Type__c = 'Elementary School';
        
        insert o;
        System.Assert(o.Id != null, 'The Test Account did not insert properly');
            
        
        Contact ct = new Contact();
        //Create new contact
        
        
        ct.RecordTypeId = [SELECT ID FROM RECORDTYPE WHERE sObjectType='CONTACT' and NAME='STUDENT/ FACULTY'].ID;
        ct.FirstName = 'TestJane';
        ct.LastName = 'TestDoe';
        ct.Aliases__c = 'Test Subject';
        ct.BirthDate = Date.newInstance(1990,1,20);
        ct.Gender__c = 'Male';
        ct.SSN__c = '900119999';
        ct.Email = 'tftl@testmail.com';
        ct.HomePhone = '4088881111';
        ct.Club_Position__c = 'Club Advisor';
        ct.Host_Organization__c = o.ID;  
        
        insert ct;
        System.Assert(ct.Id != null, 'The Test Contact did not insert properly');
        
        Club__c c = new Club__c();
        //Create new club 
        
        c.advisor__c = ct.ID;
        c.meeting_day__c = 'Monday;Tuesday';
        c.meeting_time__c = '9a - 12p;12p - 3p';
        c.host_organization__c = o.ID;
        c.club_name__c = 'Test Club';
 
        insert c;
        System.Assert(c.Id != null, 'The Test Club did not insert properly');
    }
}